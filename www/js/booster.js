angular.module('boosterapp', ['ionic', 'ngCordova', 'boosterapp.utility', 'boosterapp.services', 'boosterapp.controllers'])

.run(function($ionicPlatform, Utility, $state, $rootScope, PushNotifications) {

  var $rs = $rootScope;

  $ionicPlatform.ready(function() {
    console.log("$ionicPlatform.ready");
    //$rootScope.doOnReady();

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    console.log("CONSOLE LOG WORKS AT APP.JS RUN: ionic.Platform.ready:ionic.Platform.device()="+ionic.Platform.device());

    //navigator.splashscreen.show();

    //alert("$ionicPlatform.ready");

    $rs.setDeviceInfo(ionic.Platform.device());
    var theDp = $rs.getDevicePlatform();
    //alert('ionicPlatform.ready:theDp='+theDp);

    PushNotifications.initialize();

    /* MOVED TO LOADING CTRL
    var doAutoLogin = false;
    //alert('isFirstEntrance='+window.localStorage['isFirstEntrance']);
    if(!$rs.varIsSet(window.localStorage['isFirstEntrance']))
    {
      window.localStorage['isFirstEntrance'] = true;
      $rs.isFirstEntrance = true;      
    }
    if(window.localStorage['isFirstEntrance'] == "false")
    {
      doAutoLogin = true;
    }
    alert('doAutoLogin='+doAutoLogin);
    if(doAutoLogin == true)
    {
      $rootScope.doPromiseStartAppOnce();//.then(function(result) 
    }
    */

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.cordova.logger) {
      window.cordova.logger.__onDeviceReady();
      console.log('onDeviceReady:window.facebookConnectPlugin='+window.facebookConnectPlugin);
    }
    if(window.StatusBar) {
      if($rs.getDevicePlatform() != "Android") 
        StatusBar.setStyleForStatusBar(1);
      StatusBar.hide();
    }     
    if(window.facebookConnectPlugin) {
    }

  });

  ionic.Platform.ready(function() {
    //alert("ionic.Platform.ready");
    //$rootScope.doOnReady();
  });

})

.config(function($stateProvider, $urlRouterProvider) {
  var baseUrl = "http://lin-res.mapitpix.com/Booster/ionic_git/www/";
  $stateProvider


    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: baseUrl + "templates/menu.html",
      controller: 'MenuCtrl'
    })

    .state('app.loading', {
      url: "/loading",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/loading.html",
          controller: 'LoadingCtrl'
        }
      }
    })

    .state('app.login', {
      url: "/login",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/login.html",
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.choosebooster', {
      url: "/choosebooster",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/choosebooster.html",
          controller: 'ChooseBoostCtrl'
        }
      }
    })

    .state('app.selfie', {
      url: "/selfie/:boostId",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/selfie.html",
          controller: 'SelfieBoostCtrl'
        }
      }
    })

    .state('app.takeselfie', {
      url: "/takeselfie",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/takeselfie.html",
          controller: 'TakeSelfieBoostCtrl'
        }
      }
    })

    .state('app.editboost', {
      url: "/editboost",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/editboost.html",
          controller: 'EditBoostCtrl'
        }
      }
    })    

    .state('app.getboost', {
      url: "/getboost/:fbid",
      views: {
        'menuContent' : {
          templateUrl: baseUrl + "templates/getboost.html",
          controller: 'GetBoostCtrl'
        }
      }
    })
//          templateUrl: "http://res.theboosterapp.com/ionic_git/www/templates/upcoming.html",
    .state('app.upcoming', {
      url: "/upcoming",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/upcoming.html",
          controller: 'UpcomingCtrl'
        }
      }
    })

    .state('app.requests', {
      url: "/requests",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/requests.html",
          controller: 'RequestsCtrl'
        }
      }
    })

    .state('app.completed', {
      url: "/completed",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/completed.html",
          controller: 'CompletedCtrl'
        }
      }
    })

    .state('app.review', {
      url: "/review/:boostId/:hasAction",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/review.html",
          controller: 'ReviewCtrl'
        }
      }
    })

    .state('app.completedreview', {
      url: "/creview/:boostId/:hasAction",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/completedreview.html",
          controller: 'CompletedReviewCtrl'
        }
      }
    })

    .state('app.notification', {
      url: "/notification",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/notification.html",
          controller: 'NotificationCtrl'
        }
      }
    })

    .state('app.profile', {
      url: "/profile",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/profile.html",
          controller: 'ProfileCtrl'
        }
      }
    })
    
    .state('app.contact', {
      url: "/contact",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/contact.html"
        }
      }
    })

    .state('app.about', {
      url: "/about",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/about.html"
        }
      }
    })

    .state('app.intro1', {
      url: "/intro1",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/intro1.html"
        }
      }
    })

    .state('app.intro2', {
      url: "/intro2",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/intro2.html"
        }
      }
    })

    .state('app.intro3', {
      url: "/intro3",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/intro3.html"
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/loading');
});
angular.module('boosterapp.controllers', [])

.controller('MainCtrl', function($scope, $rootScope, $ionicModal, $ionicLoading, PushNotifications) {

  var $rs = $rootScope;

  mixpanel.track('App_Started:MainCtrl');

  //TODO: check if removing is ok
  //PushNotifications.initialize();

  $rootScope.profile={};
  $rootScope.profile.name="";
  $rootScope.profile.pic="";
  $rootScope.profile.points="";
  $rootScope.profile.level="";
  $rootScope.profile.getBoostPt="";
  $rootScope.profile.giveBoostPt="";

  $rootScope.fbUserId = "";

  //Get notfication
  $rootScope.notef = {};
  $rootScope.notef.count=0;
  $rootScope.notef.list=[];

  $rootScope.showLoading = function() {
      $ionicLoading.show({
        template: 'Please wait...'
      });
  };
  
  $rootScope.hideLoading = function(){
      $ionicLoading.hide();
  };

  $rootScope.getNotificationCount = function(){
      return ($rootScope.notef.count == 0 || $rootScope.notef.count == "") ? "0" : $rootScope.notef.count + "";
  };

  /*
  $rs.showModal=function(mod, pic, anim)
  {
      $ionicModal.fromTemplateUrl(mod, {
        scope: $rs,
        animation: anim
      }).then(function(modal) {
        $rs.modal = modal;
      });

      //Cleanup the modal when we're done with it!
      $rs.$on('$destroy', function() {
        $rs.modal.hide();
        //$rs.modal.remove();
      });
      // Execute action on hide modal
      $rs.$on('modal.hide', function() {
        // Execute action
      });
      // Execute action on remove modal
      $rs.$on('modal.removed', function() {
        // Execute action
      });
      $rs.$on('modal.shown', function() {
        console.log('Modal is shown!');
      });

      $rs.imageSrc = pic;

      $rs.modal.show();
  }
  */

  $rs.showTuts=function(b)
  {
    window.localStorage['isTutUpcoming1']      = b;
    window.localStorage['isTutUpcoming2']      = false;
    window.localStorage['didShowUpcoming2']    = false;
    window.localStorage['isTutRequests']       = b;
    window.localStorage['isTutGetBoost']       = b;
    window.localStorage['isTutChooseBooster']  = b;
    window.localStorage['isTutCompleted']      = b;
    window.localStorage['isShowTuts']          = false;
    $scope.isShowTutsAgain = false;

    $rs.isTutUpcoming1     = window.localStorage['isTutUpcoming1'];
    $rs.isTutUpcoming2     = window.localStorage['isTutUpcoming2'];
    $rs.isTutRequests      = window.localStorage['isTutRequests'];
    $rs.isTutGetBoost      = window.localStorage['isTutGetBoost'];
    $rs.isTutChooseBooster = window.localStorage['isTutChooseBooster'];
    $rs.isTutCompleted     = window.localStorage['isTutCompleted'];

  }

})

.controller('LoadingCtrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {

    var doAutoLogin = false;
    //alert('isFirstEntrance='+window.localStorage['isFirstEntrance']);
    if(!$rs.varIsSet(window.localStorage['isFirstEntrance']))
    {
      window.localStorage['isFirstEntrance'] = true;
      $rs.isFirstEntrance = true;      
    }
    if(window.localStorage['isFirstEntrance'] == "false")
    {
      doAutoLogin = true;
    }
    if(doAutoLogin == true)
    {
      $rootScope.doPromiseStartAppOnce();//.then(function(result) 
    }
    else 
    {
      $state.go('app.login');
    }

})

.controller('LoginCtrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {
    
  var $rs = $rootScope;

  $scope.facebookLogin=function()
  {
    //console.log('facebookLogin');
    $rootScope.alreadyStarting = false;
    $rootScope.doPromiseStartAppOnce();
  }

   //Handle the slide
   $scope.nextSlide = function() {
    $rootScope.trackEvent("nextSlide");
    $rootScope.trackEvent("intro_screen_num="+$ionicSlideBoxDelegate.slidesCount());
    if($ionicSlideBoxDelegate.currentIndex() == $ionicSlideBoxDelegate.slidesCount()-1){
      //$ionicSlideBoxDelegate.slide(0);  
      $rs.alreadyStarting = false;
      $rootScope.doPromiseStartAppOnce();
      //$rootScope.impl_facebookLogin();
    }
    else{
     $ionicSlideBoxDelegate.next();   
    }
  };

  $scope.$on('$viewContentLoaded', function()
  {
    //$rootScope.doPromiseStartAppOnce();
  });

})

.controller('ProfileCtrl', function($scope, $rootScope) {
  $rs = $rootScope;
  $rs.promiseGetProfile();
})

.controller('MenuCtrl', function($scope, $rootScope, Utility) {
  $rs = $rootScope;

  $scope.share = function() {
    $rs.share();
  }

  $rs.share = function() {
    $rootScope.trackEvent('MenuCtrl:share');
    var message = 'Boost me up!',
    subject = 'Booster App',
    fileOrFileArray = null,
    url = null;

    var sharing = window.plugins.socialsharing;
    sharing.share(
      message,
      subject,
      fileOrFileArray,
      url,
      function(result) {
        $rootScope.trackEvent('socialsharing');                          
        console.log('Sharing success: ' + JSON.stringify(result));
      },
      function(result) {
        $rootScope.trackError(error, "socialsharing");
        console.log('Sharing error: ' + JSON.stringify(result));
      });
  }

})

.controller('UpcomingCtrl', function(Booster, $ionicModal, $scope, $state, $rootScope, $cordovaStatusbar, Utility) { 
  var $rs = $rootScope;

  //alert('UpcomingCtrl');
  console.log('UpcomingCtrl');

  if($rs.isFirstEntrance)
  {
    console.log('isFirstEntrance');
    $rs.isFirstEntrance = false;
    //$state.go($rs.pushState);     
  } else {
  }

  $rs.showPokeOption=function(buttonIndex)
  {
    if(buttonIndex == 2) // if CANCEL, return
    {
      $rs.trackEvent('poke:cancel');
      return;
    }
    $rs.trackEvent('poke:accept');
    //alert('$rs.itemToPoke='+JSON.stringify($rs.itemToPoke));
    Booster.sendPoke($rs.itemToPoke.BoostId);
  }

  $scope.sendPoke=function(item)
  {
    var date = new Date(item.EventDate);
    //alert('sendPoke:item='+JSON.stringify(item));
    //alert('sendPoke:date='+JSON.stringify(date));
    if(item.Status != "A") {
      //alert('after you accept, you can poke')
      navigator.notification.confirm("After you accept, you can poke", null, "Poke", ["Ok"])
      return;
    }
    var ts = date.getTime();
    var now = new Date();
    var ts_now = now.getTime();
    //alert('ts='+ts);
    //alert('ts_now='+ts_now);
    if(ts < ts_now) {
      //alert('after time of event, you can poke')
      navigator.notification.confirm("After time of event, you can poke", null, "Poke", ["Ok"])
      return;
    }
    $rs.itemToPoke = item;
    navigator.notification.confirm("Send a Poke?", $rs.showPokeOption, "Poke", ["Ok", "Cancel"])
  }

  /*
  $scope.simClick = function () {
    console.log('showTutUpcoming1');
    //$timeout(function() {
      //alert('showTutUpcoming1:simClick');
      angular.element('#addboostBtn').trigger('click');
    //}, 100);
  };
  */    

  $scope.showTutUpcoming1=function(b) {
    console.log('UpcomingCtrl:showTutUpcoming1:$scope.showTut='.b);
    $rs.isTutUpcoming1 = b;
    $rs.isTutGetBoost = false; // don't show 2nd get boost till after choose booster
    window.localStorage['isTutUpcoming1'] = b;
    $state.go('app.getboost');
    //$state.transitionTo("app.getboost");
    //$state.href("app.getboost");
    //$scope.simClick();
  }

  $scope.$on('$viewContentLoaded', function()
  {
      console.log("UpcomingCtrl:$viewContentLoaded");

      $rs.isShowTutsAgain = window.localStorage['isShowTuts'];
      console.log('viewContentLoaded:$rs.isShowTutsAgain='+$rs.isShowTutsAgain);

      $rs.isTutUpcoming2 = window.localStorage['laterShowTutUpcoming2'];
      window.localStorage['laterShowTutUpcoming2'] = false;

      // $rs will change for next next time
      if(!$rs.varIsSet($rs.isShowTutsAgain) || $rs.isShowTutsAgain == "true")
      {
        console.log('run:call:showTuts:true');
        $rs.showTuts(true);
      }
      console.log('viewContentLoaded:$rs.isShowTutsAgain='+$rs.isShowTutsAgain);
      //TODO:remove
      //$rs.showTuts(true);

  });

  $scope.showTutUpcoming2=function(b) {
    //alert('UpcomingCtrl:showTutUpcoming2:$scope.showTut='+b);
    window.localStorage['isTutUpcoming2'] = b;
    $rs.isTutUpcoming2 = b;
  }

  /*
  // TODO: put in isFirstEntrance
  $scope.$on('$viewContentLoaded', 
  function(event){ 
    $rs.showModal('templates/image-modal.html', 'img/Template/PNG/new_boost.png', 'slide-in-up')
  });
  $rs.doDelayed($rs.showModal, 1000, ['templates/image-modal.html', 'img/Template/PNG/new_boost.png', 'slide-in-up']);
  */
  
  $rootScope.trackEvent('UpcomingCtrl');
  
  //Get upcoming boost
  $rootScope.showLoading();

  //Update request boost count
  $rootScope.updateRequestCount();

  //$cordovaStatusbar.show();
  var userId = $rootScope.getUserId();
  console.log("upcoming:userid=" + userId);
  Booster.getUpcomingBoost(userId).then(function(result) {
    $rootScope.hideLoading();
    console.log('UpcomingCtrl:getUpcomingBoost:$scope.list='+$rs.jStr($scope.list));
    $scope.list =result.data;      
  },
  function( errorMessage ) {
    $rootScope.hideLoading()
    $rootScope.trackError(errorMessage, "getUpcomingBoost");
    console.log('UpcomingCtrl:getUpcomingBoost:errorMessage='+errorMessage);
    alert("Error", "Error occured while getting upcoming list");
  });  

  $rootScope.updateNotifications();

})

.controller('RequestsCtrl', function($scope,$rootScope,$stateParams,$ionicPopup, Booster, Utility, $cordovaToast, $state ) {
  var $rs = $rootScope;
  $rootScope.trackEvent('RequestsCtrl');

  $scope.showTutRequests=function(b) {
    console.log('RequestsCtrl:showTutRequests:isTutRequests='+b);
    window.localStorage['isTutRequests'] = b;
    $rs.isTutRequests = b;
  }

  //Update request boost count
  $rootScope.updateRequestCount();

    //Get upcoming boost  
  $rootScope.showLoading();
  Booster.getRequestBoost($rootScope.getUserId()).then(function(result) {                   
      $rootScope.hideLoading();
      $scope.list =result.data;
      $rootScope.requestCount=result.data.length;    
    },
    function( errorMessage ) {
        $rootScope.hideLoading()
        $rootScope.trackError(errorMessage, "getRequestBoost");
        alert("Error", "Error occured while getting request list");
    }
  );  

  $scope.accept=function(id) {
    $ionicPopup.confirm({
      title: 'Confirm',
      content: 'Are you sure you want to accept?'
    }).then(function(res) {           
      if(res) {     
                $rootScope.showLoading();
                //Accepted  
                Booster.acceptBoostRequest(id).then(function(result) {                   
                    $rootScope.hideLoading()
                    //alert("Info", "Requeset accepted successfully!");                     
                      $cordovaToast.showShortCenter('Request accepted successfully!').then(function(success) {
                        $state.go('app.upcoming');
                      }, function (error) {
                        // error
                        $rootScope.trackError(error, "showShortCenter:acceptBoostRequest");
                      });

                  },
                  function( errorMessage ) {
                      $rootScope.hideLoading()                     
                      $rootScope.trackError(errorMessage, "acceptBoostRequest");
                      alert("Error", "Error occured while updateding Request");
                  }
                );                  
        }
    });       
  }

  $scope.decline=function(id) {
    $ionicPopup.confirm({
      title: 'Confirm',
      content: 'Are you sure you want to decline?' + id
    }).then(function(res) {           
      if(res) {             
                $rootScope.showLoading();            
                //Decline                
                Booster.declineBoostRequest(id).then(function(result) {                   
                    $rootScope.hideLoading()                     
                    $cordovaToast.showShortCenter('Requeset declined successfully!').then(function(success) {
                        $state.go('app.upcoming');
                      }, function (error) {
                        // error
                        $rootScope.trackError(error, "showShortCenter:decline");
                      });
                    //alert("Info", "Requeset declined successfully!");
                  },
                  function( errorMessage ) {                     
                    $rootScope.hideLoading()                     
                    alert("Error", "Error occured while updating request");
                    $rootScope.trackError(error, "declineBoostRequest");
                  }
                );                  
       }

    });       
  }

})

.controller('CompletedCtrl', function($scope,$rootScope,$stateParams,Booster , $state, Utility) {
  var $rs = $rootScope;
  $rootScope.trackEvent('CompletedCtrl');
  //Get completed list

  $scope.showTutCompleted=function(b) {
    console.log('CompletedCtrl:showTutCompleted:isTutCompleted='+b);
    $rs.isTutCompleted = b;
    window.localStorage['isTutCompleted'] = b;
  }

  $rootScope.showLoading();
  Booster.getCompletedBoost($rootScope.getUserId()).then(function(result) {                   
      $rootScope.hideLoading();
      $scope.list =result.data;
    },
    function( errorMessage ) {
        $rootScope.hideLoading()
        $rootScope.trackError(errorMessage, "getCompletedBoost");
        alert("Error", "Error occured while getting completed list");
    }
  );  
})

.controller('GetBoostCtrl', function($scope,$rootScope, $stateParams, Booster, Utility, $cordovaToast, $state) {
    console.log('GetBoostCtrl');
    var $rs = $rootScope;
    $rootScope.trackEvent('GetBoostCtrl');
    var pic = "https://graph.facebook.com/" +  $stateParams.fbid + "/picture?type=square&height=200&width=200";
    $scope.boost={};
    $scope.boost.toFbId =$stateParams.fbid;
    $scope.boost.picUrl= (($stateParams.fbid + "") == "0" ? "img/addpic.png" : pic);
    $scope.boost.comment="";
    $scope.boost.evtDateTime="Choose Date + Time";
    $scope.boost.evtTime="";
    $scope.boost.after="";

    $scope.showTutGetBoost=function(b) {
      console.log('GetBoostCtrl:showTutGetBoost:isTutGetBoost='+b);
      $rs.isTutGetBoost = b;
      window.localStorage['isTutGetBoost'] = b;
    }

    $scope.showDatetime = function()
    {
      //alert('showDatetime');
        var options = {
          date: new Date(),
          mode: 'datetime',
          allowOldDates: false
        };

        datePicker.show(options, function(dto){
          var sdatetime = dto.format('yyyy-mm-dd HH:MM');
          //alert('showDatetime:show:sdatetime = '+sdatetime);
          console.log('showDatetime:show:sdatetime='+sdatetime);
          $scope.boost.evtDateTime = sdatetime;
          $scope.$apply();
        });
    }

    $scope.addBoost=function(){
      //Check validation
      var error="";
      if($scope.boost.toFbId == 0 || $scope.boost.toFbId == "" || $scope.boost.toFbId  == null || $scope.boost.toFbId + "" == "undefined"){
         error = "Please choose a booster";
      }
      else if ($scope.boost.comment== "") {
        error = "Please enter title";
      }
      else if($scope.boost.evtDateTime == ""){
       error = "Please enter date"; 
      }
      else if($scope.boost.evtTime == ""){
       //error = "Please enter time"; 
      }
      else if($scope.boost.after == ""){
       //error = "Please selecct alert"; 
      }
      
      if(error != ""){
        $cordovaToast.showShortCenter(error).then(function(success) {
          }, function (error) {
            // error
            $rootScope.trackUserError(errorMessage, "addBoost");
          });
        return false;
      }

      var offset =new Date().getTimezoneOffset();
      //console.log('offset='+offset);

      $rootScope.showLoading();

      //var eDateTimezone = $scope.boost.evtDateTime + " " + $scope.boost.evtTime + " " + offset;
      //var eDateTimezone = $scope.boost.evtDateTime + " " + offset;
      //console.log('eDateTimezone='+eDateTimezone);
      
      ////console.log(eDateTimezone);

      Booster.addBoost($rootScope.getUserId(), $scope.boost.toFbId, $scope.boost.evtDateTime, offset,  $scope.boost.comment, $scope.boost.after).then(function(result) {                   
          $rootScope.hideLoading();          
          $cordovaToast.showShortCenter('Boost Added Successfully!').then(function(success) {
            if(!$rs.varIsSet(window.localStorage['didShowUpcoming2']) || window.localStorage['didShowUpcoming2'] == "true") 
            {
              window.localStorage['laterShowTutUpcoming2'] = false;
            } else {
              window.localStorage['laterShowTutUpcoming2'] = true;
              window.localStorage['didShowUpcoming2'] = true;          
            }
            $state.go('app.upcoming');
          }, function (error) {
                        // error
            $rootScope.trackError(error, "addBoost");
          });
          //alert("Info", "Boost created successfully");
      },
      function( errorMessage ) {
          $rootScope.hideLoading()
          $rootScope.trackError(errorMessage, "addBoost");
          alert("Error", "Error occured while adding boost");
      }

      );                    
    }
})

.controller('NotificationCtrl', function($location, $state, $scope,$rootScope, Booster, Utility) {
    var $rs = $rootScope;
    $rootScope.trackEvent('NotificationCtrl');
    $rootScope.showLoading();

     Booster.markNotfRead($rootScope.getUserId()).then(function(result) {  
          $rootScope.hideLoading(); 
          $rootScope.notef.count=0;         
      },
      function( errorMessage ) {
          $rootScope.hideLoading()
          $rootScope.trackError(errorMessage, "NotificationCtrl:markNotfRead");
          alert("Error", "Error occured while NotificationCtrl, adding boost");
      }

      );     

    $scope.showDetail=function(item){
      if(item.Type == "AcceptBoost"){
        $state.go("app.upcoming");
      }
      else if(item.Type == "RejectBoost"){
        $state.go("app.upcoming");
      }
      else if(item.Type == "Selfie"){
       $location.path("/app/review/" + item.TargetId + "/true"); 
      }
      else if(item.Type == "BoostRequest"){
        $state.go("app.requests");
      }
      else if(item.Type == "Review"){
        $location.path("/app/creview/" + item.TargetId + "/false"); 
      }
      else if(item.Type == "UpdateBoost"){
        $state.go("app.upcoming");
      }

    }        
})

.controller('ChooseBoostCtrl', function($scope, $rootScope, $location, Booster, Utility) {
  var $rs = $rootScope;
  $rootScope.trackEvent("ChooseBoostCtrl");
  $rootScope.showLoading();
  $scope.boosters = [];
  $scope.$on('$viewContentLoaded', function(event) { 
    tryGetFacebookFriends();
  });

  $rs.showTutChooseBooster=function(b) {
    console.log('ChooseBoostCtrl:$scope.showTutChooseBooster='+$scope.showTut);
    $rs.isTutChooseBooster = b;
    window.localStorage['isTutChooseBooster'] = b;
    $rs.isTutGetBoost = true;
  }

  $scope.invite=function(){
    $rs.share();    
  }

  $scope.selectBoost=function(booster){  
    $location.path("/app/getboost/" + booster.id )
  }


  var tryGetFacebookFriends = function() { 
    //Check if connected
    getFriends();
    /*
    facebookConnectPlugin.getLoginStatus(
        function(res){
            if(res.status + "" == "connected"){ 
              getFriends();    
            }
            else{
              //TODO: Login
              //Get frined
            }      
        },
        function(err){
          alert("err : " + err);
        }
      );
    */
  }

  //Merge with server default booster
  var addDefaultBooster=function(boosters){
      $rootScope.trackEvent("addDefaultBooster");
      
      var currentFbIb= $rootScope.getFBUserId();

      Booster.getDefaultBoosters().then(function(result) {  
        // TODO: change 90 to server side behavior to always return the def boosters if it is not
        for(var i=0 ; i < result.data.length && boosters.length < 90  ; i++ )
        {
            //Check if already exist in list
            var isExist=false;
            for(var x =0 ; x < boosters.length ; x++)
            {
              if(boosters[x].id ==  result.data[i].FbUserId || result.data[i].FbUserId == currentFbIb   )
              {
                isExist =true;
                break                
              }
            }

            if(!isExist)
            {
                var item=
                {
                  "name" : result.data[i].Name,
                  "picUrl" : result.data[i].PicUrl,
                  "id" : result.data[i].FbUserId
                }

                boosters.unshift(item);
            }
        }

        //$scope.$apply(function() {     
        //  $scope.boosters = boosters;
        //});
      },
      function( errorMessage ) {
        $rootScope.trackError(errorMessage, "getDefaultBoosters");
        alert("Error", "Error occured while getting boost details for id : " + $stateParams.boostId);
      });  
    
  }

  var getFriends = function() {
    //if($rs.getDevicePlatform()=="Android")
      facebookConnectPlugin.api($rootScope.getFBUserId() + "/friends?fields=id,name,picture", ["user_friends"],
      //facebookConnectPlugin.makeGraphCall($rootScope.getFBUserId() + "/friends?fields=id,name,picture", ["user_friends"],
        function (result) {
       
          //console.log('getFriends');
          $rootScope.trackEvent("getFriends");
          response =result;
          var boosters=[];

            if(response.data == null || response.data == undefined || response.data.length <= 0)
            {
              addDefaultBooster(boosters);
            }
            else
            {
              for(var i=0 ; i < response.data.length ; i++ )
              {
                var item=
                {
                  "name" : response.data[i].name,
                  "picUrl" : response.data[i].picture.data.url,
                  "id" : response.data[i].id
                }

                boosters.push(item);
              }
            }

            $scope.boosters = boosters;//response.data;   
            if(boosters.length < 90 || $rs.isTutCompleted)
              addDefaultBooster(boosters);

          $rootScope.hideLoading();
        
        },
        function (error) {
            alert("Failed: " + error);
        });
  };
})

.controller('SelfieBoostCtrl', function($scope,$rootScope, $stateParams, Booster, Utility) {
  $rootScope.trackEvent("SelfieBoostCtrl");
    
  $rootScope.showLoading();
  
  Booster.getBoostDetails($stateParams.boostId).then(function(result) {                   
    $rootScope.hideLoading();      
    console.log("Booster.getBoostDetails:$rootScope.details="+JSON.stringify($rootScope.details));
    $rootScope.details =result.data;    
    $scope.details = result.data;
    
  },
  function( errorMessage ) {
    $rootScope.hideLoading()
    $rootScope.trackError(errorMessage, "getBoostDetails");
    alert("Error", "Error occured while getting boost details for id : " + $stateParams.boostId);
  });  

})

.controller('ReviewCtrl', function($scope, $state, Booster, $rootScope, $stateParams,$cordovaToast, Booster, Utility) {
    $rootScope.trackEvent("ReviewCtrl");
    $rs = $rootScope;

    $scope.selfie={};
    $scope.selfie.selfieId= "";

    $rootScope.showLoading();
    Booster.getSelfie($stateParams.boostId).then(function(result) {                   
      $rootScope.hideLoading();
      $scope.selfie =result.data;    
      
      if($rs.getUserId() == $scope.selfie.UserId)
      {
        $scope.selfie.iAmBooster = 'true';
      }      

      $scope.selfie.selfieId= result.data.SelfieId;
      $scope.selfie.hasAction=$stateParams.hasAction;

      $scope.rcFbShare=function()
      {
        console.log("rcFbShare:$scope.selfie.UserId="+JSON.stringify($scope.selfie.UserId));
        var selfie = $scope.selfie;
        //selfie.ImageData = selfie.image;
        selfie.message = "@"+selfie.RequestUserName + " boosted me to " + selfie.BoostTitle;
        $rs.fbShare(selfie);
      }
    },
    function( errorMessage ) {
      $rootScope.hideLoading()
      $rootScope.trackError(errorMessage, "getSelfie");
      alert("Error", "Error occured while gettting selfie for id:" + $stateParams.boostId);
    });  

    $scope.review={};
    $scope.review.comment="";

    $scope.sendReview=function(isLovedIt){
      $rootScope.showLoading();
      Booster.addBoostReview($scope.selfie.selfieId, $scope.review.comment, isLovedIt).then(function(result) {                   
        $state.go('app.completed');
         $cordovaToast.showShortCenter('Review sent successfully!').then(function(success) {                        
          }, function (error) {
            // error
            $rootScope.trackError(error, "addBoostReview");
          });
        },
        function( errorMessage ) {
          $rootScope.hideLoading()
          $rootScope.trackError(errorMessage, "addBoostReview");
          alert("Error", "Error occured while sending review for selfie id:" + $scope.selfie.selfieId);
        });  
    }

    $scope.back=function(){
      $state.go('app.completed');
    }

})

.controller('CompletedReviewCtrl', function($scope, $state, $stateParams, $rootScope, Booster, Utility, $cordovaToast) {
  $rs = $rootScope;
    $rootScope.trackEvent("CompletedReviewCtrl");
    $rootScope.showLoading();
    Booster.getSelfie($stateParams.boostId).then(function(result) {                   
      $rootScope.hideLoading();
      $rootScope.details =result.data;          
      $scope.selfie =result.data;    
      $scope.selfie.isMySelfie = false;
      console.log('CompletedReviewCtrl:getSelfie:$scope.selfie.isMySelfie='+$scope.selfie.isMySelfie);
      if($rs.getUserId() == result.data.UserId)
      {
        $scope.selfie.isMySelfie = 'true';
      }
      console.log('CompletedReviewCtrl:$scope.selfie.isMySelfie='+$scope.selfie.isMySelfie);

      $scope.crcfbShare=function()
      {
        console.log('CompletedReviewCtrl:sendSelfie:fbShare:$scope.selfie.UserId'+$scope.selfie.UserId);
        var selfie = $scope.selfie;
        //selfie.ImageData = selfie.image;
        selfie.message = "@"+$rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
        //selfie.message = "@"+$rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
        $rootScope.fbShare(selfie);
      }
   
    },
    function( errorMessage ) {
      $rootScope.hideLoading()
      $rootScope.trackError(errorMessage, "getSelfie");
      alert("Error", "Error occured while gettting selfie for id:" + $stateParams.selfieId);
    }); 

    $scope.done=function(){
      $state.go('app.completed');
    } 

})

.controller('TakeSelfieBoostCtrl', function($scope,$rootScope, $state, $cordovaCamera, $cordovaToast, Booster, Utility ) {
  $rootScope.trackEvent("TakeSelfieBoostCtrl");
  $rs = $rootScope;
  /*
  $scope.sendAndShareSelfie = function(){
    $scope.sendSelfie();
    //TODO: call as thenable
    $scope.fbShare();
  }
  */

  $scope.sendSelfie = function(doShare){
    $rootScope.trackEvent("sendSelfie");
      $rootScope.showLoading();   
      Booster.sendSelfie($rootScope.details.BoostId, $scope.selfie.comment, $scope.selfie.image   ).then(function(result) {                   
        $rootScope.hideLoading();      
         $cordovaToast.showShortCenter('Selfie sent successfully!').then(function(success) {
            if(doShare==true) {
              console.log('TakeSelfieBoostCtrl:sendSelfie:fbShare:$scope.selfie.UserId'+$scope.selfie.UserId);
              var selfie = $scope.selfie;
              selfie.ImageData = selfie.image;
              $scope.details = $rs.details;
              selfie.message = "@"+$rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
              $rs.fbShare(selfie).then(function(result){
                $state.go('app.completed');
              });
            }
            $state.go('app.completed');
          }, function (error) {
                        // error
            $rootScope.trackError(error, "showShortCenter:send_selfie");
          });
      },
      function( errorMessage ) {
        $rootScope.hideLoading()
        $rootScope.trackError(errorMessage, "sendSelfie");
        alert("Error", "Error occured when sending selfie");
      });  

    }

    $scope.selfie={};
    $scope.selfie.comment="";
    $scope.selfie.image ="";
    
    var options = { 
        quality : 75, 
        destinationType : Camera.DestinationType.DATA_URL, 
        sourceType : Camera.PictureSourceType.CAMERA, 
        targetWidth: 1920,
        targetHeight: 0,
        encodingType: Camera.EncodingType.JPEG,
        mediaType : 0,
        allowEdit : true,
        correctOrientation : true,
        saveToPhotoAlbum : false,
        popoverOptions: CameraPopoverOptions,
        cameraDirection: 1,

    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      //console.log(JSON.stringify(imageData));
      $scope.selfie.image=imageData;
      // Success! Image data is here
    }, function(err) {
      // An error occured. Show a message to the user
      $rootScope.trackError(err, "getPicture");
    });

    // code for customized camera plugin
    // navigator.customCamera.getPicture('photo.png', function success(fileUri) {
    // }, function failure(error) {
    // }, {
    //     quality: 80,
    //     targetWidth: 120,
    //     targetHeight: 120,
    //     topText: 'Your booster: Justin Bieber'
    // });
  
})

.controller('EditBoostCtrl', function($scope,$rootScope,$ionicSideMenuDelegate) {
    $rootScope.trackEvent("EditBoostCtrl");
})angular.module('boosterapp.services', ['ionic'])

	.service('Booster', function($http, $cordovaDevice, $rootScope) {
	var $rs = $rootScope;
	//var baseUrl = "http://devb.theboosterapp.com/api/";
	var baseUrl = "http://mapapin.com/api/";
	//var baseUrl = "http://ws.theboosterapp.com/api/";

	this.register = function(fbId, firstName, lastName, email, dob) {
	
		var picUrl= "https://graph.facebook.com/" +  fbId + "/picture?type=square&height=200&width=200";

		return $http({
				method: 'POST',
				url: baseUrl + "User/Register",
				data: {
						FbUserId: fbId,
                        Email: email,
                        FirstName: firstName,
                        LastName: lastName,
                        PicUrl: picUrl
                    }
			});	
	}

	this.updateDeviceId = function(userId, deviceId) {	    
		
		var qString="userId=" + userId + "&deviceId=" + deviceId + "&platform=" + $rs.getDevicePlatform(); //Note: for android "A", Ios "I";
		var theUrl = baseUrl + "User/UpdateDeviceId?" +  qString;
		console.log('updateDeviceId:theUrl='+theUrl);
		return $http({
				method: 'GET',
				url: theUrl
			});	
		
	}

	this.getProfileByFbId = function(fbId) {	
		console.log('services.getProfileByFbId:fbId='+fbId);    
		var qString="fbUserId=" + fbId ;
		return $http({
				method: 'GET',
				url: baseUrl + "User/GetProfileByFbUserId?" +  qString
			});	
		
	}
	
	this.getUpcomingBoost = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetUpcommingBoost?userId=" + userId
			});		
	}

	this.getRequestBoostCount = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetBoostRequestCount?userId=" + userId
			});		
	}

	this.getRequestBoost = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetBoostRequest?userId=" + userId
			});		
	}
	

	this.acceptBoostRequest = function(boostID) {		
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/AcceptBoost?boostId=" + boostID
			});		
	}	

	this.declineBoostRequest = function(boostId) {		
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/RejectBoost?boostId=" + boostId
			});		
	}



	this.getCompletedBoost = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetCompletedBoost?userId=" + userId
			});	
	}


	this.addBoost = function(fromUserId, toUserId, date, time, title, alert) {    		
		return $http({
				method: 'POST',
				url: baseUrl + "Boost/CreateBoostRequest",
				data: {
						UserId: fromUserId,
                        ToFbUserId: toUserId,
                        Title: title,
                        EventDate: date,
                        TzOffset: time,
                        Alert: alert
                    }
		});	
	}

	this.getBoostDetails = function(boostId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetBoostDetail?boostId=" + boostId
			});	
	}

	this.markNotfRead = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/MarkNotfRead?userId=" + userId
			});	
	}

	this.updateBoost = function(boostId, fromUserId, toUserId, dateTime, alert) {
		return $http({
				method: 'POST',
				url: baseUrl + "AddBoost",
				data: {
						boostId: boostId,
                        fromUserId: fromUserId,
                        toUserId: toUserId,
                        dateTime: dateTime,
                        alert: alert
                    }
			});	
	}

	this.addBoostReview = function(selfieId, comment, isLovedIt) {
		return $http({
				method: 'POST',
				url: baseUrl + "Boost/AddSelfieReview",
				data: {
                        BoostSelfieId: selfieId,
                        Comment: comment,
                        LovedIt: isLovedIt
                    }
			});	
	}

	this.sendSelfie = function(boostId, comment, imageData) {    		
		return $http({
				method: 'POST',
				url: baseUrl + "Boost/SendSelfie",
				data: {
						BoostId: boostId,
                        Comment: comment,
                        Image: imageData,
                    }
		});	
	}

	this.getSelfie = function(boostId) {
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetSelfieDetails?boostId=" + boostId
			});	
	}

	this.getNotification = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetNotficatios?userId=" + userId
			});	
	}

	this.sendPoke = function(boostId) {
		var theUrl = baseUrl + 'Boost/SendPoke?boostId='+boostId;
		//alert('sendPoke:theUrl=')+theUrl;
		return $http({
				method: 'GET',
				url: theUrl
			});	
	}

	this.getDefaultBoosters = function() {	    
		return $http({
				method: 'GET',
				url: baseUrl + "User/GetDefaultBooster"
			});	
	}


})

.service('PushNotifications', function(Utility, $ionicPlatform, $cordovaToast, $rootScope, $state) {

	var $rs = $rootScope;
	//alert('PushNotifications');

	var pushPlugin = null;

	/*
	this.deviceRegId = function() {
		$rootScope.getFDeviceId();
	}
	*/	

	this.initialize = function() {

		//alert('PushNotifications:initialize');

		//document.addEventListener('deviceready', function() {
		//$ionicPlatform.ready(function() {
			//console.log('PushNotifications:initialize:deviceready:device='+JSON.stringify(device));
			//$rs.setDeviceInfo(device);
			pushPlugin = window.plugins.pushNotification;
			///alert('pushPlugin='+pushPlugin);

			//TODO:!! FIX THIS
			//$rs.setDeviceInfo(JSON.stringify(ionic.Platform.device()));
			var dp = $rs.getDevicePlatform();
			//alert('initialize:$rs.dp='+dp);
			//alert('initialize:$rs.getDevicePlatform()='+$rs.getDevicePlatform());
			//TODO : solve undefined
			if ( dp == 'android' || dp == 'Android' || dp == "amazon-fireos" )
			{
				//alert('pushPlugin:not-ios:call:register:dp='+dp);
				pushPlugin.register(
					function(result) {          
						//alert('PushNotifications:initialize:2:result='+result);
						console.log('PushNotifications:initialize:1:result='+result);
					},
					function(result) {
						alert('PushNotifications:initialize:2:result='+result);
						console.log('PushNotifications:initialize:2:result='+JSON.stringify(result));
					},
					{
						"senderID":"220739435041",
						"ecb":"onNotificationGCM"
					});
				
			} else if ( dp == 'iOS' ) {
				//alert('pushPlugin:ios:call:register:dp='+dp);
				pushPlugin.register(
					function(result) {
						//  alert('pushPlugin.register:1:result='+JSON.stringify(result));
						console.log('pushPlugin.register:1:result='+JSON.stringify(result));
						$rootScope.setDeviceId(result)
					},
					function(result) {
						//alert('PushNotifications:initialize:2:result='+result);
						console.log('pushPlugin.register:2:result='+JSON.stringify(result));
					},
					{
						"badge":"true",
						"sound":"true",
						"alert":"true",
						"ecb":"onNotificationAPN"
					});
			}

		//});

		// notifications for Android
		window.onNotificationGCM = function(e) {
			//alert('onNotificationGCM:e='+JSON.stringify(e));
			window.boosterNotification = e;						
			switch( e.event )
		    {
		    case 'registered':
		        if ( e.regid.length > 0 )
		        {
		          $rootScope.setDeviceId(e.regid);
		        }
		    break;

		    case 'message':
		        // if this flag is set, this notification happened while we were in the foreground.
		        // you might want to play a sound to get the user's attention, throw up a dialog, etc.
		        if ( e.foreground )
		        {
		            // on Android soundname is outside the payload. 
		            // On Amazon FireOS all custom attributes are contained within payload
		            var soundfile = e.soundname || e.payload.sound;
		            // if the notification contains a soundname, play it.
		            //var my_media = new Media("/android_asset/www/"+ soundfile);
		            //my_media.play();
		        }
		        else
		        {  // otherwise we were launched because the user touched a notification in the notification tray.
		            if ( e.coldstart )
		            {
		                //
		            }
		            else
		            {
		                //
		            }
		        }

		        //navigator.notification.confirm("message", $rs.handleNotification, ["test title"], ["Ok", "Cancel"])
				
				$rs.sendToScreen(e.payload);

				/*
				var msg = e.payload.message.replace(/<b>/g, "")
				msg = msg.replace(/<\/b>/g, "");
				$cordovaToast.showShortCenter(msg).then(function(success) {
                        //$state.go('app.upcoming');
                        $rootScope.updateNotifications();
                      }, function (error) {
                        // error
                	}
                );
				*/

		       //console.log(e.payload.message); 
		       //Only works for GCM
		       // e.payload.msgcnt + '</li>');
		       //Only works on Amazon Fire OS
		       // e.payload.timeStamp
		    break;

		    case 'error':
		        //e.msg 
		    break;

		    default:
		        // Unknown
		    break;
		  }

		};

		// notifications for iOS
		window.onNotificationAPN = function(result) {
			//alert('onNotificationAPN:result='+JSON.stringify(result));
		    //console.log('onNotificationAPN:result='+JSON.stringify(result));

			/*
			if ( event.alert )
		    {
		        //navigator.notification.alert(event.alert);
		    }

		    if ( event.sound )
		    {
		        //var snd = new Media(event.sound);
		        //snd.play();
		    }

		    if ( event.badge )
		    {
		        //.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
		    }
		    */

		    $rs.sendToScreen(result);

		};

	};

	$rs.handleNotification=function(buttonIndex)
	{
		console.log('handleNotification:buttonIndex='+buttonIndex);

		if(buttonIndex == 2) // if CANCEL, return
		{
			return;
		}

		// else, goto screen
		if( $rs.varIsSet($rs.isAppStarted) )
		{
			if($rs.varIsSet($rs.pushState))
			{
				if($rs.varIsSet($rs.pushTarget))
				{
					$state.go($rs.pushState, {boostId: $rs.pushTarget});
				} else 
				{
					$state.go($rs.pushState);
				}
			}
		}
	}

	$rs.sendToScreen = function(result)
	{
		console.log('new sendToScreen:result='+JSON.stringify(result));
		var nType = result.nType;
		var targetId = result.targetId;
		var firstName = result.firstName;
		var boostName = result.boostName;
		console.log('sendToScreen:nType='+nType);
		console.log('sendToScreen:targetId='+targetId);
		console.log('sendToScreen:firstName='+firstName);
		console.log('sendToScreen:boostName='+boostName);

		var msg;
		if(nType == "AcceptBoost") {
			//r = window.confirm(''+firstName + ' accepted your request for '+boostName+'! Check it now?');
			msg = firstName + ' accepted your request for '+boostName+'! Check it now?';
			$rs.pushState = 'app.upcoming';
		} else if(nType == "Poke") {
			msg = firstName + ' Poked you to '+boostName+'! Check it now?';
			$rs.pushState = 'app.upcoming';
		} else if (nType == "RejectBoost") {
			msg = firstName + ' rejected your request for '+boostName+'! Check it now?';
		    $rs.pushState = 'app.upcoming';
		} else if (nType == "BoostRequest") {
			msg = firstName + ' request a boost to '+boostName+'! Check it now?';
		    $rs.pushState = 'app.requests';
		} else if (nType == "Selfie") {
			msg = firstName + 'sent you a selfi for '+boostName+'! Check it now?';
		    $rs.pushState = 'app.review';
		    $rs.pushTarget= result.targetId;
		} else if (nType == "Review") {
			msg = firstName + ' sent you a review for '+boostName+'! Check it now?';
		    $rs.pushState = 'app.completedreview';
		    $rs.pushTarget= result.targetId;
		} else if (nType == "UpdateBoost") {
			msg = firstName + ' send you an update for '+boostName+'! Check it now?';
		    $rs.pushTarget= result.targetId;
		} else if (nType == "Time") {
			msg = 'It is time for '+firstName+' to '+boostName+'! Check it now?';
		}

		console.log("sendToScreen:$rs.pushState="+$rs.pushState);
		console.log("sendToScreen:$rs.pushTarget="+$rs.pushTarget);

		window.boosterNotification = result;

		navigator.notification.confirm(msg, $rs.handleNotification, nType, ["Ok", "Cancel"])

	}	


});

angular.module('boosterapp.utility', ['ionic'])

	.service('Utility', function($ionicPopup, $q, $rootScope, $state, $cordovaToast, Booster) {

    var $rs = $rootScope;

    //var self = this;    
    var debug = 3; // debug level - higher means more verbose messages
	$rs.popup = function(title, message) {	    
		 $ionicPopup.show({
		     title: title,
		     template: message
		 });
	}

    $rs.goToState=function(theState) {
        $state.go(theState);
    }

    $rs.doDelayed=function(f1, delay, args)
    {
        console.log('doDelayed:args='+JSON.stringify(args));
        function sleep(millis, callback) {
            setTimeout(function()
                    { callback(); }
            , millis);
        }
        function foobar_cont(){
            console.log("foobar_cont");
            console.log('doDelayed:foobar_cont:args='+JSON.stringify(args));
            f1.apply(this, args);
        };
        sleep(delay, foobar_cont);    
    }

    $rs.fbShare=function(selfie){
        var d = $q.defer();
      console.log('$rs.fbShare:selfie.UserId='+selfie.UserId);
      $rootScope.trackEvent("fbShare");

      var message = selfie.message,//selfie.BoostTitle,
      subject = 'Booster App',
      fileOrFileArray = "data:image/png;base64," + selfie.ImageData ,
      url = "http://signup.theboosterapp.com";

      var sharing = window.plugins.socialsharing;
      sharing.share(
      message,
      subject,
      fileOrFileArray,
      url,
      function(result) {
        $rootScope.trackEvent('User shared');                          
        console.log('Sharing success: ' + JSON.stringify(result));
        d.resolve(result);
        return d.promise;
      },
      function(result) {
        $rootScope.trackError(error, "fbShare");
        console.log('Sharing error: ' + JSON.stringify(result));
        d.reject(result);
        return d.promise;
      });
      return d.promise;
    }

    //FB login button click event
    $rs.promiseFacebookLogin = function() 
    {
        console.log('promiseFacebookLogin');
        var fbId = $rs.getFBUserId();
        var fbLr = $rs.fbLoginResponse;
        var d = $q.defer();
        console.log('promiseFacebookLogin:2:fbLr='+$rs.jStr(fbLr));
        //var fbId = window.localStorage['fbid'];
        if($rs.varIsSet(fbId) && $rs.varIsSet(fbLr)) 
        {
            //mixpanel.identify(fbId);
            console.log('promiseFacebookLogin:resolve:fbId='+fbId);
            console.log('promiseFacebookLogin:resolve:fbLr='+fbLr);
            d.resolve(fbId);
            return d.promise;
        }

        /*
        console.log('window.cordova='+window.cordova);
        if (!window.cordova) {
            //facebookConnectPlugin = fbcpWeb;
            facebookConnectPlugin.browserInit("625654127541709");
            // version is optional. It refers to the version of API you may want to use.
        }
        */
 
        console.log('promiseFacebookLogin:call:facebookConnectPlugin');
        facebookConnectPlugin.
        login
        (
            ['public_profile', 'user_friends', 'email'], 
            // SUCCESS
            function(res) 
            {
                console.log("promiseFacebookLogin:res=" + $rs.jStr(res));
                $rs.setFbLoginResponse(res);
                d.resolve(res);
                return d.promise;
        }, 
            function(err) 
            {
                var errLog = 'promiseFacebookLogin:facebookConnectPlugin:err'+err;
                console.log(errorLog);
                d.reject(errLog);
                return d.promise;
            }       
        )
        console.log('promiseFacebookLogin:end');
        return d.promise;
    };

    $rs.promiseGetLoginStatus = function()
    {
        console.log('promiseGetLoginStatus');
        var d = $q.defer();
        console.log('promiseGetLoginStatus:d='+d);
        //d.notify('promiseGetLoginStatus');

        var fbLr = $rs.getFbLoginResponse();
        console.log('promiseGetLoginStatus:fbLr='+fbLr);
        if($rs.varIsSet(fbLr)) 
        {
            d.resolve(fbLr);
            return d.promise;
        }

        /*
        console.log('window.cordova='+window.cordova);
        if (!window.cordova) {
            //facebookConnectPlugin = fbcpWeb;
            facebookConnectPlugin.browserInit("625654127541709");
            // version is optional. It refers to the version of API you may want to use.
        }
        */

        facebookConnectPlugin.getLoginStatus
        (
            function(res)
            {
                var fbId;
                console.log('promiseGetLoginStatus:getLoginStatus:res='+$rs.jStr(res));
                if(res.status + "" == "connected")
                { 
                    fbId = res.authResponse.userID;
                    if($rs.varIsSet(fbId))
                    {
                        $rs.setFBUserId(fbId);
                        console.log('promiseGetLoginStatus:getLoginStatus:resolve:fbId='+fbId);
                        d.resolve(res);
                        return d.promise;
                }
                    else
                    {
                        console.log('promiseGetLoginStatus:getLoginStatus:reject:fbId=='+fbId);
                        d.reject();
                        return d.promise;
                    }
                } else
                {
                    console.log('promiseGetLoginStatus:getLoginStatus:reject:connected==false');
                    d.reject();
                    return d.promise;
                }
            },
            function(err){
                console.log('facebookConnectPlugin.getLoginStatus:err='+err);
            }
        )
        console.log('promiseGetLoginStatus:getLoginStatus:return');
        return d.promise;
    };

    $rs.promiseGetProfile = function()
    {
        console.log('promiseGetProfile');
        var d = $q.defer();
        var fbId = $rs.getFBUserId();
        console.log('promiseGetProfile:fbId='+fbId);
        if(!$rs.varIsSet(fbId))
        {
            console.log('promiseGetProfile:reject:fbId='+fbId);
            d.reject('fbId='+fbId);
            return d.promise;
        }
        Booster.getProfileByFbId(fbId)
        .then(function(result)
        {
            console.log('promiseGetProfile:getProfileByFbId:result='+$rs.jStr(result));
            $rs.getProfileRes = result;
            if($rs.varIsSet(result.data))
            {
                console.log('promiseGetProfile:getProfileByFbId:resolve');
                $rs.setProfileFromWs(result);                
                d.resolve(result);
                return d.promise;
            }
            else
            {
                console.log('promiseGetProfile:getProfileByFbId:reject');
                d.reject(result);
                /*
                console.log('promiseGetProfile:return:promiseRegister');
                return $rs.promiseRegister()
                .then(function(result)
                {
                    console.log('promiseGetProfile:return:promiseGetProfile');
                    return $rs.promiseGetProfile();
                    //d.reject(result);
                });
                */
            }
        }).catch(function(err){
            console.log('catch:promiseGetProfile:getProfileByFbId:reject');
            d.reject(err);
            return d.promise;
        });
        return d.promise;
    }

    $rs.promiseRegister = function()
    {
        var d = $q.defer();
        /*
        if(window.localStorage['isRegistered']=="true")
        {
            d.resolve();
            return d.promise;
        }
        */
        var fbLr = $rs.getFbLoginResponse();
        //var fbMe = $rs.fbGraphMeRes;
        if(!$rs.varIsSet(fbLr))
        {
            console.log('promiseRegister:reject:fbLr='+fbLr);
            d.reject(data);
            return d.promise;
        }
        var data = fbLr.authResponse;
        console.log('promiseRegister:data='+$rs.jStr(data));
        Booster.register(data.id, data.first_name, data.last_name, data.email,"")
        .then(function(result)
        {
            console.log('promiseRegister:register:result='+$rs.jStr(result));
            if(result.data.IsSuccess==true)
            {   
                window.localStorage['isRegistered']=true;
                console.log('promiseRegister:register:resolve');
                d.resolve(result);
            }
            else
            {
                console.log('promiseRegister:register:reject');
                d.reject(result);
            }
        },
        function(error){
            console.log('promiseRegister:register:reject:error='+error);
            d.reject(error);
        });
        d.reject();
        return d.promise;
    }

    $rs.doPromiseStartAppOnce = function()
    {
        //alert('doPromiseStartAppOnce');
        console.log('doPromiseStartAppOnce');
        var d = $q.defer();
        if($rs.alreadyStarting == true)
        {
            window.localStorage['isFirstEntrance'] = false;
            d.reject($rs.alreadyStarting);
            return false;
        }
        $rs.alreadyStarting = true;
        // TODO: need this but js wasn't finding it, even though I'm merely defining it here
        $rs.startAppCallCount = 1;
        //alert('doPromiseStartAppOnce:startAppCallCount='+startAppCallCount);
        //console.log('doPromiseStartAppOnce:startAppCallCount='+startAppCallCount);
        window.localStorage['isFirstEntrance'] = false;

        return $rs.promiseStartApp();
        return d.promise;
    }

    // RECURSIVE WORK BACKWARDS
    $rs.promiseStartApp = function()
    {
        //alert('promiseStartApp');
        var d = $q.defer();
        var doStart = true;
        console.log('promiseStartApp - promiseGetProfile')
        return $rs.promiseGetProfile()
        .catch(function(err){
            doStart=false;
            console.log('promiseStartApp - promiseRegister')
            return $rs.promiseRegister()
        // }).catch(function(err){
            // doStart=false;
            // console.log('promiseStartApp - promiseGetFbGraphMe')
            // return $rs.promiseGetFbGraphMe()
        }).catch(function(err){
            doStart=false;
            console.log('promiseStartApp - promiseFacebookLogin')
            return $rs.promiseFacebookLogin()
        }).finally(function(result){
            console.log('promiseStartApp - doStart='+doStart);
            if(doStart) {
                console.log('promiseStartApp - resolve:call:startUpcoming='+result);
                $rs.startUpcoming();
                if($rs.varIsSet($rs.pushState))
                {
                    $state.go($rs.pushState);  
                    $rs.pushState = "";    
                }
                $rs.isAppStarted = true;

                d.resolve(result);
                return d.promise;
            } else {
                // RECURSE
                console.log('promiseStartApp - call:self:RECURSE:result='+result);
                // TODO, some prob with this startAppCallCount variable
                if($rs.startAppCallCount > 10)
                {
                    alert('promiseStartApp - call:self:RECURSE:startAppCallCount='+startAppCallCount+' result='+result);
                    d.reject(result);
                    return d.promise;
                }
                $rs.startAppCallCount++;
                $rs.promiseStartApp();
            }
        });
        return d.promise;
    };

    /*
    $rs.promiseStartAppProc = function()
    {
        console.log('promiseStartApp');
        var d = $q.defer();
        return $rs.promiseGetLoginStatus()
        .catch(function(err){
            return $rs.promiseFacebookLogin()
        //}).then(function(result){
        //    return $rs.promiseGetFbGraphMe()
        }).then(function(result){
            return $rs.promiseGetProfile()
        //}).then(function(result){
        //    return $rs.promiseAddDefaultBooster()
        }).then(function(result){
            console.log('promiseStartApp:resolve:result='+result);
            $rs.startUpcoming();
            d.resolve(result);
            return d.promise;
        }).catch(function(err){
            console.log('promiseStartApp:reject:err='+err);
        });
        return d.promise;
    };
    */

    $rs.startUpcoming = function() 
    {
        console.log('startUpcoming');
        var getProfileWsr = $rs.getProfileFromWs();
        getProfileWsr = JSON.parse(getProfileWsr);
        console.log('startUpcoming:getProfileWsr='+$rs.jStr(getProfileWsr));
        $rs.setUserId(getProfileWsr.data.UserId);
        $rs.updateDeviceId();
        $rs.setProfileFromWs(getProfileWsr);                
        $state.go('app.upcoming');
    }

    $rs.varIsSet = function(x)
    {
        if(x == "" || x == null || x == undefined || x == "null" || x == "undefined")
        {
            return false;
        }
        return true;
    }

    /*
    $rs.promiseGetFbGraphMe = function() {
        var d = $q.defer();
        console.log('promiseGetFbGraphMe');
        var d = $q.defer();
        var fbId = $rs.getFBUserId();
        if(!$rs.varIsSet(fbId))
        {
            console.log('promiseGetFbGraphMe:reject:fbId='+fbId);
            d.reject(fbId);
            return d.promise;
        }
        console.log('promiseGetFbGraphMe:fbId='+fbId);
        facebookConnectPlugin.api(fbId + "?fields=id,name,first_name,last_name,email,picture",
            ['public_profile'],
        function (result) {
            console.log("promiseGetFbGraphMe:api:result="+$rs.jStr(result));
            $rs.fbGraphMeRes=result;
            d.resolve(result);
            return d.promise;
        },
        function (error) {
            console.log('promiseGetFbGraphMe:api:error='+error);
            d.reject(error);
            return d.promise;
        });
        console.log('promiseGetFbGraphMe:return');
        return d.promise;
    };
    */

    //Update device id  
    $rs.updateDeviceId=function(){    
        var userId = $rs.getUserId();
        //console.log('updateDeviceId:userId='+userId);
        $rs.trackEvent("updateDeviceId");
        var did = $rs.getDeviceId();
        console.log('updateDeviceId:userId'+userId+' did='+did);
        Booster.updateDeviceId(userId, did).then(function(response) {                   
                    //Device updated successfully
                    console.log('updateDeviceId:response='+response);
                    var test =response;
                  },
                  function( errorMessage ) {                
                    $rs.trackError(errorMessage, "updateDeviceId");
                    console.log('updateDeviceId:errorMessage='+$rs.jStr(errorMessage));
                    //$rs.popup("Error", "Error occured in updateDeviceId");
                  }

        );
    };

    $rs.updateNotifications= function(){
        console.log('updateNotifications');
        Booster.getNotification($rs.getUserId()).then(function(result) { 
            $rs.notef.count =result.data.UnReadCount; 
            $rs.notef.list =result.data.Notifications;      
          },
          function( errorMessage ) {
            $rs.popup("Error", "Error: updateNotifications");
        });   

        $rs.updateRequestCount();
    }

    $rs.updateRequestCount=function(){
         //Update request count 
        Booster.getRequestBoostCount($rs.getUserId()).then(function(result) { 
            $rs.requestCount =result.data; 
               
          },
          function( errorMessage ) {
            $rs.popup("Error", "Error: updateRequestCount");
        }); 

    }

    $rs.setDevicePlatform=function(dp)
    {
        $rs.devicePlatform=dp;
    }

    $rs.getDevicePlatform=function()
    {
        //console.log('getDevicePlatform='+$rs.devicePlatform);
        return $rs.devicePlatform;
    }

    $rs.trackEvent = function(act) {
        var d = "x";
        d = $rs.getDevicePlatform();
        if(d == "Android") d = "A";
        if(d == "iOS") d = "i";

        mixpanel.track(d+":"+act);

        //GA 
        ga('send', 'screenview', {
          'appName': 'BoosterClassic',
          'appId': 'myAppId',
          'appVersion': '1.0',
          'appInstallerId': 'myInstallerId',
          'screenName': act
        });
    }

    $rs.bulog = function(msg) {
        console.log("ul:"+msg);
        /*
        $cordovaToast.showShortTop('Here is a message').then(function(success) {
            // success
        }, function (error) {
            // error
        });
        */        
    }

    $rs.trackError = function(act) {
        $rs.trackEvent("Error:"+act);
    }

    $rs.trackUserError = function(act) {
        $rs.trackEvent("UserError:"+act);
    }

    $rs.getFBUserId = function(){        	       	
        return window.localStorage['fbid']; 
    };
        
    $rs.setFBUserId = function(fbid){     
        //console.log('setFBUserId:fbid='+fbid);   	
        window.localStorage['fbid'] = fbid;              
    };

    $rs.getFbFriends = function(){     
        var fbFriends = window.localStorage['fbFriends'];             
        //console.log('getFbFriends:fbFriends='+fbFriends);             
        return fbFriends;  
    };

    $rs.setFbFriends = function(fbFriends){     
        //console.log('setFbFriends:fbFriends='+$rs.jStr(fbFriends));             
        window.localStorage['fbFriends'] = $rs.jStr(fbFriends);              
    };

    $rs.getUserId = function(){            
        return window.localStorage['userId'];  
    };
        
    $rs.setUserId = function(userId){          
        window.localStorage['userId'] = userId;              
    };

    $rs.getProfileFromWs = function(){     
        console.log('getProfileWsr');             
        return window.localStorage['profile'];  
    };
        
    /*
    ** @result = ws result
    */
    $rs.setProfileFromWs = function(getProfileWsr){    
        //console.log('setProfileFromWs:getProfileWsr='+$rs.jStr(getProfileWsr));      
        window.localStorage['profile'] = $rs.jStr(getProfileWsr);       

        //Need to set first time tood
        $rs.profile = [];
        $rs.profile.name = getProfileWsr.data.Name;
        $rs.profile.points = getProfileWsr.data.GiveBoostPoint + getProfileWsr.data.GetBoostPoint;
        $rs.profile.level = getProfileWsr.data.Level;
        $rs.profile.getBoostPt=getProfileWsr.data.GetBoostPoint;
        $rs.profile.giveBoostPt=getProfileWsr.data.GiveBoostPoint;
        $rs.profile.pic = getProfileWsr.data.PicUrl;

        $rs.setMixpanelProfile(getProfileWsr);

    };

    $rs.setMixpanelProfile=function(data)
    {
        //alert("setMixpanelProfile:$rs.getDevicePlatform()="+$rs.getDevicePlatform());
        mixpanel.people.set({
            "points": $rootScope.profile.points,
            "level": $rootScope.profile.level,
            "getBoostPt": $rootScope.profile.getBoostPt,
            "giveBoostPt": $rootScope.profile.giveBoostPt,
            "pic": $rootScope.profile.pic,
            "device": JSON.stringify($rs.getDevicePlatform()),
        });

        mixpanel.identify(data.id);
        mixpanel.alias(data.id);

        mixpanel.register_once({
            "referred_id": 123,
            "UserId": data.UserId,

        });

        mixpanel.register({
            "fb_data": data,
        });
    }

    $rs.getFbLoginResponse = function()
    {
        return $rs.fbLoginResponse;
    }
    
    $rs.setFbLoginResponse = function(res)
    {
        console.log('setFbLoginResponse:res='+$rs.jStr(res));
        //var fbId = res.authResponse.userID;
        var fbId = res.authResponse.id;
        console.log('setFbLoginResponse:fbId='+fbId);
        $rs.setFBUserId(fbId);
        $rs.fbLoginResponse = res;
    }

    $rs.setDeviceInfo = function(myDevice) 
    {
        $rs.setDevicePlatform(myDevice.platform);
    };

    $rs.getDeviceId = function(){         
        var deviceId = window.localStorage['did']; 
        console.log('getDeviceId:deviceId='+deviceId);
        return deviceId;
    };
        
    $rs.setDeviceId = function(deviceId){          
        $rs.myDeviceId = deviceId;     
        var before = window.localStorage['did'];    
        window.localStorage['did'] = deviceId;
        // If we just acquired a deviceId, where it was null before
        //if(!$rs.varIsSet(before) && $rs.varIsSet(deviceId)) 
        //{
            console.log('setDeviceId:updateDeviceId');
            $rs.updateDeviceId();                     
        //}
        console.log('setDeviceId:deviceId='+deviceId);
    };

    $rs.jStr = function(jsonObj)
    {
        return JSON.stringify(jsonObj, null, "\t");
    }

    $rs.PEDNING="PENDING";
    $rs.REQUEST="REQUEST";
    $rs.CIMPLITED="CIMPLITED"
});