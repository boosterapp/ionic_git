angular.module('boosterapp.controllers', [])

.controller('MainCtrl', function($scope, $rootScope, $state, $ionicModal, $ionicLoading, PushNotifications) {

    var $rs = $rootScope;

    //mixpanel.track('App_Started:MainCtrl');

    //TODO: check if removing is ok
    //PushNotifications.initialize();

    $rs.fbUserId = "";

    //Get notfication
    $rs.notef = {};
    $rs.notef.count = 0;
    $rs.notef.list = [];

    $rs.showLoading = function() {
        var theTimeout = 5000;
        if ($state.$current == "app.notification") {
            theTimeout = 1000;
        }
        $ionicLoading.show({
            template: 'Please wait...',
            duration: theTimeout
        });
    };

    $rs.hideLoading = function() {
        $ionicLoading.hide();
    };

    $rs.getNotificationCount = function() {
        return ($rs.notef.count == 0 || $rs.notef.count == "") ? "0" : $rs.notef.count + "";
    };

    $rs.showTuts = function(b) {
        window.localStorage['isTutUpcoming1'] = b;
        window.localStorage['isTutUpcoming2'] = false;
        window.localStorage['didShowUpcoming2'] = false;
        window.localStorage['isTutRequests'] = b;
        window.localStorage['isTutGetBoost'] = b;
        window.localStorage['isTutChooseBooster'] = b;
        window.localStorage['isTutCompleted'] = b;
        window.localStorage['isShowTuts'] = false;
        $scope.isShowTutsAgain = false;

        $rs.isTutUpcoming1 = window.localStorage['isTutUpcoming1'];
        $rs.isTutUpcoming2 = window.localStorage['isTutUpcoming2'];
        $rs.isTutRequests = window.localStorage['isTutRequests'];
        $rs.isTutGetBoost = window.localStorage['isTutGetBoost'];
        $rs.isTutChooseBooster = window.localStorage['isTutChooseBooster'];
        $rs.isTutCompleted = window.localStorage['isTutCompleted'];

    }

    $scope.getClassBell = function() {
        var rv = $rs.getCssClass('gb-bell-icon');
        return rv;
    }
    $scope.getClassIonContent = function() {
        var rv = $rs.getCssClass('gb-ion-content');
        return rv;
    }

    document.addEventListener("resume", onResume, false);

    function onResume() {
        $rs.$broadcast("MainCtrl:resume");
    }

})

.controller('LoadingCtrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {
    var $rs = $rootScope;
    $rs.trackEvent('LoadingCtrl');
    // alert('LoadingCtrl');
    $rs.callPromiseStartAppOnce();
})

.controller('AboutCtrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {
    var $rs = $rootScope;
    $rs.trackEvent('AboutCtrl');
    $scope.boosterVersion = $rs.boosterVersion;
})

// AFTER FB LOGIN
.controller('Loading2Ctrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {
    var $rs = $rootScope;
    $rs.trackEvent('Loading2Ctrl');
    // alert('Loading2Ctrl');
    $rs.doPromiseStartAppOnce();
})

.controller('LoginCtrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {

    var $rs = $rootScope;
    $rs.trackEvent('LoginCtrl');

    $scope.facebookLogin = function() {
        $state.go('app.loading2');
    }

    //Handle the slide
    $scope.nextSlide = function() {
        $rs.trackEvent("intro_screen_num=" + $ionicSlideBoxDelegate.currentIndex());
        if ($ionicSlideBoxDelegate.currentIndex() == $ionicSlideBoxDelegate.slidesCount() - 1) {
            $state.go('app.loading2');
        } else {
            $ionicSlideBoxDelegate.next();
        }
    };

})

.controller('ProfileCtrl', function($scope, $rootScope) {
    $rs = $rootScope;
    //$rs.promiseGetProfile();
})

.controller('MenuCtrl', function($scope, $rootScope, Utility) {
    $rs = $rootScope;

    $scope.share = function() {
        $rs.trackEvent('MenuCtrl:fbShare:result');
        $rs.share();
    }

    $rs.share = function() {
        $rs.trackEvent('MenuCtrl:share');
        var message = "I'm using Booster App. Download it here!\n\nApp Store:\nhttps://itunes.apple.com/il/app/booster-app/id920384966?mt=8&uo=4\n\nGoogle play:\nhttps://play.google.com/store/apps/details?id=com.theboosterapp.booster.android\n\nGet ready to boost your life!!",
            subject = 'Booster App',
            fileOrFileArray = null,
            url = null;

        var sharing = window.plugins.socialsharing;
        sharing.share(
            message,
            subject,
            fileOrFileArray,
            url,
            function(result) {
                $rs.trackEvent('socialsharing:1');
                if ($rs.debug > 0)
                    console.log('Sharing success: ' + JSON.stringify(result));
            },
            function(result) {
                $rs.trackError(error, "socialsharing:2");
                if ($rs.debug > 0)
                    console.log('Sharing error: ' + JSON.stringify(result));
            });
    }

})

.controller('UpcomingCtrl', function(Booster, $ionicModal, $scope, $state, $rootScope, $cordovaStatusbar, Utility) {
    var $rs = $rootScope;
    $scope.onResume = function() {
        if ($rs.debug > 0) console.log('UpcomingCtrl');
        // $scope.profile = $rs.profile;
        if ($rs.debug > 3) console.log("UpcomingCtrl:$scope.profile=" + JSON.stringify($scope.profile));

        if ($rs.isFirstEntrance) {
            //PushNotifications.initialize();
            if ($rs.debug > 0)
                console.log('isFirstEntrance');
            $rs.isFirstEntrance = false;
            //$state.go($rs.pushState);     
        } else {}

        $rs.trackEvent('ComingUpPage');
        //Get upcoming boost
        $rs.showLoading();

        //Update request boost count
        $rs.updateRequestCount();

        //$cordovaStatusbar.show();
        var userId = $rs.getUserId();
        if ($rs.debug > 0)
            console.log("upcoming:userid=" + userId);
        Booster.getUpcomingBoost(userId).then(function(result) {

                $rs.hideLoading();
                if ($rs.debug > 0)
                    console.log('UpcomingCtrl:getUpcomingBoost:$scope.list=' + $rs.jStr($scope.list));
                $scope.list = result.data;

                $rs.upcomingListLength = $scope.list.length;
                // SHOW TUT 2
                if (window.localStorage['addBoostCount'] == 1 && $rs.upcomingListLength == 1) {
                    $scope.isTutUpcoming2 = true;
                }
                //$scope.$apply();

                $scope.showTutUpcoming1();
            },
            function(errorMessage) {
                $rs.hideLoading()
                $rs.trackError(errorMessage, "getUpcomingBoost");
                if ($rs.debug > 0)
                    console.log('UpcomingCtrl:getUpcomingBoost:errorMessage=' + errorMessage);
                if ($rs.debug > 0)
                    console.log("Error in getting upcoming list", "Error occured while getting upcoming list");
            });

        $rs.updateNotifications();
    }

    $scope.onResume();

    $scope.getClassBar = function() {
        var rv = $rs.getCssClass('gb-bar');
        return rv;
    }
    $scope.getClassBell = function() {
        var rv = $rs.getCssClass('gb-bell-icon');
        return rv;
    }
    $scope.getClassIonContent = function() {
        var rv = $rs.getCssClass('gb-ion-content');
        return rv;
    }

    $rs.getCssClass = function(cssClassName) {
        var rv = cssClassName + "-" + $rs.getDevicePlatform();
        return rv;
    }

    $rs.showPokeOption = function(buttonIndex) {
        if (buttonIndex == 2) // if CANCEL, return
        {
            $rs.trackEvent('poke:cancel');
            return;
        }
        $rs.trackEvent('poke:accept');
        //if($rs.debug>0) console.log('$rs.itemToPoke='+JSON.stringify($rs.itemToPoke));
        Booster.sendPoke($rs.itemToPoke.BoostId);
    }

    $scope.sendPoke = function(item) {

        var date = new Date(item.EventDate);
        if (item.Status != "A") {
            //if($rs.debug>0) console.log('after you accept, you can poke')
            navigator.notification.confirm("After you accept, you can Boost", null, "Poke", ["Ok"])
            return;
        }
        var ts = date.getTime();
        var offset = new Date().getTimezoneOffset();
        ts += offset * 60 * 1000;
        var now = new Date();
        var ts_now = now.getTime();
        if ($rs.debug > 0)
            console.log('ts=' + ts);
        if ($rs.debug > 0)
            console.log('ts_now=' + ts_now);
        if (ts > ts_now) {
            if ($rs.debug > 0)
                console.log('You can Boost only after the time of the event');
            navigator.notification.confirm("You can Boost only *after* the time of the event", null, "Boost", ["Ok"])
            return;
        }
        $rs.itemToPoke = item;
        navigator.notification.confirm("Give a Boost?", $rs.showPokeOption, "Boost", ["Ok", "Cancel"])
    }

    $scope.showTutUpcoming1 = function() {
        var ul = $scope.list;
        var ull = ul.length;
        if ($rs.debug > 0) console.log('UpcomingCtrl:showTutUpcoming1:$scope.showTut='.b);
        if (ull > 0) {
            $scope.pngPath = "";
        } else {
            $scope.pngPath = "img/tut_newboost.png";
        }
    }

    $scope.showTutUpcoming2 = function(b) {
        //if($rs.debug>0) console.log('UpcomingCtrl:showTutUpcoming2:$scope.showTut='+b);
        window.localStorage['addBoostCount'] = 2;
        window.localStorage['isTutUpcoming2'] = b;
        $scope.isTutUpcoming2 = b;
        $rs.isTutUpcoming2 = b;
    }

    $rs.$on('MainCtrl:resume', function(event) {
        $scope.onResume();
    });

})

.controller('CompletedCtrl', function($scope, $rootScope, $stateParams, Booster, $state, Utility) {
    var $rs = $rootScope;
    $rs.trackEvent('CompletedCtrl');
    //Get completed list

    $scope.onResume = function() {
        //Update request boost count
        $scope.profile = $rs.profile;
        if ($rs.debug > 3) console.log("CompletedCtrl:$scope.profile=" + JSON.stringify($scope.profile));

        $rs.updateRequestCount();

        $rs.showLoading();
        Booster.getCompletedBoost($rs.getUserId()).then(function(result) {
                $rs.hideLoading();
                $scope.list = result.data;
                // $scope.$apply();
            },
            function(errorMessage) {
                $rs.hideLoading()
                $rs.trackError(errorMessage, "getCompletedBoost");
                if ($rs.debug > 0)
                    console.log("Error", "Error occured while getting completed list");
            });
    }

    $scope.onResume();

    $scope.getClassIonContent = function() {
        var rv = $rs.getCssClass('gb-ion-content-completed');
        return rv;
    }

    $scope.showTutCompleted = function(b) {
        if ($rs.debug > 0)
            console.log('CompletedCtrl:showTutCompleted:isTutCompleted=' + b);
        $rs.isTutCompleted = b;
        window.localStorage['isTutCompleted'] = b;
    }

    $rs.$on('MainCtrl:resume', function(event) {
        $scope.onResume();
    });

})

.controller('GetBoostCtrl', function($scope, $rootScope, $stateParams, Booster, Utility, $cordovaToast, $state) {
    var $rs = $rootScope;
    if ($rs.debug > 0)
        console.log('GetBoostCtrl');
    $rs.trackEvent('AddBoostPage');
    var pic = "https://graph.facebook.com/" + $stateParams.fbid + "/picture?type=square&height=200&width=200";
    $scope.boost = {};
    $scope.boost.toFbId = $stateParams.fbid;
    $scope.boost.picUrl = (($stateParams.fbid + "") == "0" ? "img/addpic.png" : pic);
    $scope.boost.comment = "";
    $scope.boost.evtDateTime = "Choose Date + Time";
    $scope.boost.evtTime = "";
    $scope.boost.after = "";

    $scope.showTutGetBoost = function(b) {
        event.preventDefault();
        event.stopPropagation();
        if ($rs.debug > 0)
            console.log('GetBoostCtrl:showTutGetBoost:isTutGetBoost=' + b);
        $rs.isTutGetBoost = b;
        window.localStorage['isTutGetBoost'] = b;
    }

    $scope.showDatetime = function() {
        $rs.trackEvent("showDatetime");
        if ($rs.devicePlatform == "iOS") {
            //if($rs.debug>0) console.log('showDatetime');
            var options = {
                date: new Date(),
                mode: 'datetime',
                allowOldDates: false,
                backgroundAlpha1: 0,
                backgroundAlpha2: 0.7,
            };

            datePicker.show(options, function(dto) {
                var sdatetime = dto.format('yyyy-mm-dd HH:MM');
                if ($rs.debug > 0)
                    alert('showDatetime:show:sdatetime=' + sdatetime);
                $scope.boost.evtDateTime = sdatetime;
                $scope.$apply();
            });
            datePicker._dateChanged = function(timeIntervalSince1970) {
                console.log("_dateChanged:timeIntervalSince1970=" + timeIntervalSince1970);
                //$rs.trackEvent("_dateChanged");
                $scope.updateDateTime(timeIntervalSince1970);
            }
            datePicker._dateSelectionCanceled = function() {
                console.log("_dateSelectionCanceled");
                //$rs.trackEvent("_dateSelectionCanceled");
                $scope.updateDateTime(timeIntervalSince1970);
            }
            datePicker._dateSelected = function(timeIntervalSince1970) {
                console.log("_dateSelected:timeIntervalSince1970=" + timeIntervalSince1970);
                //$rs.trackEvent("_dateSelected");
                $scope.updateDateTime(timeIntervalSince1970);
            }
            $scope.updateDateTime = function(timeIntervalSince1970) {
                var ts = timeIntervalSince1970 * 1000;
                var dto = new Date();
                dto.setTime(ts);
                var sdatetime = dto.format('yyyy-mm-dd HH:MM');
                $scope.boost.evtDateTime = sdatetime;
                $scope.$apply();
            }

        } else { //if($rs.devicePlatform == "Android") {
            //if($rs.debug>0) console.log('showDatetime');
            var options = {
                date: new Date(),
                mode: 'date',
                allowOldDates: false
            };
            datePicker.show(options, function(dto) {
                var sdate = dto.format('yyyy-mm-dd ');
                if ($rs.debug > 0)
                    console.log('showDatetime:show:sdate=' + sdate);
                $scope.boost.evtDateTime = sdate;
                //$scope.$apply();

                var optionsTime = {
                    date: new Date(),
                    mode: 'time',
                    allowOldDates: true
                };
                datePicker.show(optionsTime, function(dto2) {
                    var stime = dto2.format('HH:MM');
                    if ($rs.debug > 0)
                        console.log('showDatetime:show:stim=' + stime);
                    $scope.boost.evtDateTime += stime;
                    $scope.$apply();
                });
            });
        }
    }

    $scope.addBoost = function() {
        //Check validation
        var error = "";
        if ($scope.boost.toFbId == 0 || $scope.boost.toFbId == "" || $scope.boost.toFbId == null || $scope.boost.toFbId + "" == "undefined") {
            error = "Please choose a booster";
        } else if ($scope.boost.comment == "") {
            error = "Please enter title";
        } else if ($scope.boost.evtDateTime == "") {
            error = "Please enter date";
        } else if ($scope.boost.evtTime == "") {
            //error = "Please enter time"; 
        } else if ($scope.boost.after == "") {
            //error = "Please selecct alert"; 
        }

        if (error != "") {
            $cordovaToast.showShortCenter(error).then(function(success) {}, function(error) {
                // error
                $rs.trackUserError(errorMessage, "addBoost");
            });
            return false;
        }

        var offset = new Date().getTimezoneOffset();
        //if($rs.debug>0) console.log('offset='+offset);

        $rs.showLoading();

        //var eDateTimezone = $scope.boost.evtDateTime + " " + $scope.boost.evtTime + " " + offset;
        //var eDateTimezone = $scope.boost.evtDateTime + " " + offset;
        //if($rs.debug>0) console.log('eDateTimezone='+eDateTimezone);

        ////if($rs.debug>0) console.log(eDateTimezone);

        Booster.addBoost($rs.getUserId(), $scope.boost.toFbId, $scope.boost.evtDateTime, offset, $scope.boost.comment, $scope.boost.after).then(function(result) {
                $rs.hideLoading();
                $cordovaToast.showShortCenter('Boost Added Successfully!').then(
                    function(success) {
                        if (!$rs.varIsSet(window.localStorage['addBoostCount'])) {
                            window.localStorage['addBoostCount'] = 1;
                        } else {
                            window.localStorage['addBoostCount'] ++;
                        }
                        $state.go('app.upcoming');
                    },
                    function(error) {
                        // error
                        $rs.trackError(error, "addBoost");
                    });
                //if($rs.debug>0) console.log("Info", "Boost created successfully");
            },
            function(errorMessage) {
                $rs.hideLoading()
                $rs.trackError(errorMessage, "addBoost");
                if ($rs.debug > 0)
                    console.log("Error", "Error occured while adding boost");
            }

        );
    }
})

.controller('NotificationCtrl', function($location, $state, $scope, $rootScope, Booster, Utility) {
    var $rs = $rootScope;
    $rs.trackEvent('NotificationCtrl');
    $rs.showLoading();
    $scope.result;

    $scope.onResume = function() {
        Booster.markNotfRead($rs.getUserId()).then(
            function(result) {
                if ($rs.debug > 0)
                    console.log('markNotfRead:result=' + JSON.stringify(result));
                $rs.hideLoading();
                $rs.notef.count = 0;
                $scope.result = result;
            },
            function(errorMessage) {
                $rs.hideLoading()
                $rs.trackError(errorMessage, "NotificationCtrl:markNotfRead");
                console.log("Error", "Error occured while NotificationCtrl, adding boost");
            });
    }

    $scope.onResume();

    $scope.getClass = function(index) {
        var item = $rs.notef.list[index];
        if ($rs.debug > 6) console.log('NotificationCtrl:getClass:item.Type=' + item.Type);
        var classes = {};
        if (item.IsRead) {
            classes.oldNotif = true;
            if ($rs.debug > 7) console.log('NotificationCtrl:getClass:classes.oldNotif=' + classes.oldNotif);
        } else {
            classes.newNotif = true;
            if ($rs.debug > 7) console.log('NotificationCtrl:getClass:classes.newNotif=' + classes.newNotif);
        }
        classes[item.Type] = true;
        if ($rs.debug > 6) console.log('classes[item.Type]=' + classes[item.Type]);
        return classes;
    }

    $scope.showDetail = function(item) {
        if (item.Type == "AcceptBoost") {
            $state.go("app.upcoming");
        } else if (item.Type == "RejectBoost") {
            $state.go("app.upcoming");
        } else if (item.Type == "Selfie") {
            $location.path("/app/review/" + item.TargetId + "/true");
        } else if (item.Type == "BoostRequest") {
            $state.go("app.upcoming");
        } else if (item.Type == "Review") {
            $location.path("/app/creview/" + item.TargetId + "/false");
        } else if (item.Type == "UpdateBoost") {
            $state.go("app.upcoming");
        }
    }

    $rs.$on('MainCtrl:resume', function(event) {
        $scope.onResume();
    });

})

.controller('ChooseBoostCtrl', function($scope, $rootScope, $q, $location, Booster, Utility) {
    var $rs = $rootScope;
    $rs.trackEvent("ChooseBoostCtrl");
    $rs.showLoading();
    $scope.boosters = [];
    $scope.$on('$viewContentLoaded', function(event) {
        tryGetFacebookFriends();
    });

    //    $rs.showTutChooseBooster = function(b) {
    //        if ($rs.debug > 0)
    //            console.log('ChooseBoostCtrl:$scope.showTutChooseBooster=' + $scope.showTut);
    //        $rs.isTutChooseBooster = b;
    //        window.localStorage['isTutChooseBooster'] = b;
    //        $rs.isTutGetBoost = true;
    //    }

    $scope.invite = function() {
        $rs.trackEvent('ChooseBoostCtrl:invite');
        $rs.share();
    }

    $scope.selectBoost = function(booster) {
        $location.path("/app/getboost/" + booster.id)
    }


    var tryGetFacebookFriends = function() {
        //Check if connected
        $rs.getFriends();
        /*
         facebookConnectPlugin.getLoginStatus(
         function(res){
         if(res.status + "" == "connected"){ 
         getFriends();    
         }
         else{
         //TODO: Login
         //Get frined
         }      
         },
         function(err){
         if($rs.debug>0) console.log("err : " + err);
         }
         );
         */
    }

    //Merge with server default booster
    var addDefaultBooster = function(boosters) {
        $rs.trackEvent("addDefaultBooster");

        var currentFbIb = $rs.getFBUserId();

        Booster.getDefaultBoosters().then(function(result) {
                // TODO: change 90 to server side behavior to always return the def boosters if it is not
                for (var i = 0; i < result.data.length && boosters.length < 90; i++) {
                    //Check if already exist in list
                    var isExist = false;
                    for (var x = 0; x < boosters.length; x++) {
                        if (boosters[x].id == result.data[i].FbUserId || result.data[i].FbUserId == currentFbIb) {
                            isExist = true;
                            break
                        }
                    }

                    if (!isExist) {
                        var item = {
                            "name": result.data[i].Name,
                            "picUrl": result.data[i].PicUrl,
                            "id": result.data[i].FbUserId
                        }

                        boosters.unshift(item);
                    }
                }

                //$scope.$apply(function() {     
                //  $scope.boosters = boosters;
                //});
            },
            function(errorMessage) {
                $rs.trackError(errorMessage, "getDefaultBoosters");
                if ($rs.debug > 0)
                    console.log("Error", "Error occured while getting boost details for id : " + $stateParams.boostId);
            });

    }

    $scope.handleInvite = function(buttonIndex) {
        if ($rs.debug > 0) console.log('handleInvite:buttonIndex=' + buttonIndex);
        if (buttonIndex == 2) // if CANCEL, return
        {
            $rs.trackEvent('noFriends:invite:cancel');
            return;
        }
        $rs.trackEvent('noFriends:invite:ok');
        $rs.share();
    }

    $scope.showTutNoFriends = function() {
        //        alert('showTutNoFriends');
        $scope.noFriendsPngPath = "img/tut_noFriends.png";
        navigator.notification.confirm("Invite friends to Boost you!",
            $scope.handleInvite, "Invite Friends", ["Ok", "Cancel"]);
    }

    $rs.getFriends = function() {
        var d = $q.defer();
        //if($rs.getDevicePlatform()=="Android")
        facebookConnectPlugin.api($rs.getFBUserId() + "/friends?fields=id,name,picture", ["user_friends"],
            //facebookConnectPlugin.makeGraphCall($rs.getFBUserId() + "/friends?fields=id,name,picture", ["user_friends"],
            function(result) {

                //if($rs.debug>0) console.log('getFriends');
                $rs.trackEvent("getFriends");
                response = result;
                var boosters = [];
                var boosterFbIds = [];

                //                alert('response.data.length='+response.data.length);
                if (response.data == null || response.data == undefined || response.data.length <= 0) {
                    $scope.showTutNoFriends();
                    //addDefaultBooster(boosters);
                } else {
                    for (var i = 0; i < response.data.length; i++) {
                        var item = {
                            "name": response.data[i].name,
                            "picUrl": response.data[i].picture.data.url,
                            "id": response.data[i].id
                        }

                        boosters.push(item);
                        boosterFbIds.push(item.id);
                    }
                }

                $scope.boosters = boosters; //response.data;   
                if (boosters.length <= 0) // || $rs.isTutCompleted)
                {
                    $scope.showTutNoFriends();
                    //addDefaultBooster(boosters);
                }

                $rs.hideLoading();

                d.resolve(boosterFbIds);

            },
            function(error) {
                if ($rs.debug > 0)
                    console.log("Failed: " + error);
                d.reject(error);
            });
        return d.promise;
    };
})

.controller('SelfieBoostCtrl', function($scope, $rootScope, $stateParams, Booster, Utility) {
    var $rs = $rootScope;
    $rs.trackEvent("SelfieBoostCtrl");

    $rs.showLoading();

    Booster.getBoostDetails($stateParams.boostId).then(function(result) {
            $rs.hideLoading();
            if ($rs.debug > 0)
                console.log("Booster.getBoostDetails:$rs.details=" + JSON.stringify($rs.details));
            $rs.details = result.data;
            $scope.details = result.data;

        },
        function(errorMessage) {
            $rs.hideLoading()
            $rs.trackError(errorMessage, "getBoostDetails");
            if ($rs.debug > 0)
                console.log("Error", "Error occured while getting boost details for id : " + $stateParams.boostId);
        });

})

.controller('ReviewCtrl', function($scope, $state, Booster, $rootScope, $stateParams, $cordovaToast, Booster, Utility) {
    $rs = $rootScope;

    $rs.trackEvent("ReviewCtrl");

    $scope.selfie = {};
    $scope.selfie.selfieId = "";

    var uid = $rs.getUserId();
    if (uid == 1640 || uid == 1681 || uid == 1641) {
        //alert('ReviewCtrl:$stateParams='+JSON.stringify($stateParams));
    }
    console.log('ReviewCtrl:$stateParams=' + JSON.stringify($stateParams));

    $rs.showLoading();
    Booster.getSelfie($stateParams.boostId).then(function(result) {
            $rs.hideLoading();
            $scope.selfie = result.data;

            if ($rs.getUserId() == $scope.selfie.UserId) {
                $scope.selfie.iAmBooster = 'true';
            }

            $scope.selfie.selfieId = result.data.SelfieId;
            $scope.selfie.hasAction = $stateParams.hasAction;

            $scope.rcFbShare = function() {
                if ($rs.debug > 0)
                    console.log("rcFbShare:$scope.selfie.UserId=" + JSON.stringify($scope.selfie.UserId));
                var selfie = $scope.selfie;
                //selfie.ImageData = selfie.image;
                selfie.message = "@" + selfie.RequestUserName + " boosted me to " + selfie.BoostTitle;
                $rs.trackEvent('ReviewCtrl:fbShare');
                $rs.fbShare(selfie);
            }
        },
        function(errorMessage) {
            $rs.hideLoading()
            $rs.trackError(errorMessage, "getSelfie");
            if ($rs.debug > 0)
                console.log("Error", "Error occured while gettting selfie for id:" + $stateParams.boostId);
        });

    $scope.review = {};
    $scope.review.comment = "";

    $scope.sendReview = function(isLovedIt) {
        $rs.showLoading();
        Booster.addBoostReview($scope.selfie.selfieId, $scope.review.comment, isLovedIt).then(function(result) {
                $state.go('app.completed');
                $cordovaToast.showShortCenter('Review sent successfully!').then(function(success) {
                    $rs.trackReview($scope.review.comment, isLovedIt);
                }, function(error) {
                    // error
                    $rs.trackError(error, "addBoostReview");
                });
            },
            function(errorMessage) {
                $rs.hideLoading()
                $rs.trackError(errorMessage, "addBoostReview");
                if ($rs.debug > 0)
                    console.log("Error", "Error occured while sending review for selfie id:" + $scope.selfie.selfieId);
            });
    }

    $scope.back = function() {
        $state.go('app.completed');
    }

})

.controller('CompletedReviewCtrl', function($scope, $state, $stateParams, $rootScope, Booster, Utility, $cordovaToast) {
    $rs = $rootScope;
    $rs.trackEvent("CompletedReviewCtrl");
    $rs.showLoading();
    Booster.getSelfie($stateParams.boostId).then(function(result) {
            $rs.hideLoading();
            $rs.details = result.data;
            $scope.selfie = result.data;
            $scope.selfie.isMySelfie = false;
            if ($rs.debug > 0)
                console.log('CompletedReviewCtrl:getSelfie:$scope.selfie.isMySelfie=' + $scope.selfie.isMySelfie);
            if ($rs.getUserId() == result.data.UserId) {
                $scope.selfie.isMySelfie = 'true';
            }
            if ($rs.debug > 0)
                console.log('CompletedReviewCtrl:$scope.selfie.isMySelfie=' + $scope.selfie.isMySelfie);

            $scope.crcfbShare = function() {
                if ($rs.debug > 0)
                    console.log('CompletedReviewCtrl:sendSelfie:fbShare:$scope.selfie.UserId' + $scope.selfie.UserId);
                var selfie = $scope.selfie;
                //selfie.ImageData = selfie.image;
                selfie.message = "@" + $rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
                //selfie.message = "@"+$rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
                $rs.trackEvent('CompletedReviewCtrl:fbShare');
                $rs.fbShare(selfie);
            }

        },
        function(errorMessage) {
            $rs.hideLoading()
            $rs.trackError(errorMessage, "getSelfie");
            if ($rs.debug > 0)
                console.log("Error", "Error occured while gettting selfie for id:" + $stateParams.selfieId);
        });

    $scope.done = function() {
        $state.go('app.completed');
    }

})

.controller('TakeSelfieBoostCtrl', function($scope, $q, $rootScope, $state, $cordovaCamera, $cordovaToast, $cordovaFile, Booster, Utility) {
    $rs = $rootScope;
    $rs.trackEvent("TakeSelfieBoostCtrl");

    $scope.selfie = {};
    $scope.selfie.comment = "";
    $scope.selfie.image = "";

    var dp = $rs.getDevicePlatform();
    var theTargetWidth = window.innerWidth;
    //alert('theTargetWidth='+theTargetWidth);
    if (theTargetWidth <= 240) theTargetWidth = 240;

    var options = {
        quality: 20,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: theTargetWidth,
        targetHeight: theTargetWidth,
        encodingType: Camera.EncodingType.JPEG,
        mediaType: 0,
        allowEdit: true,
        saveToPhotoAlbum: false,
        correctOrientation: false,
        popoverOptions: CameraPopoverOptions,
        cameraDirection: 1
    };

    //alert('dp='+dp);
    if (dp == "iOS") {
        var options = {
            quality: 90,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            targetWidth: theTargetWidth,
            targetHeight: theTargetWidth,
            encodingType: Camera.EncodingType.JPEG,
            mediaType: 0,
            correctOrientation: false,
            saveToPhotoAlbum: false,
            popoverOptions: CameraPopoverOptions,
            cameraDirection: 1
        };
        //alert('options='+JSON.stringify(options));
    }

    $cordovaCamera.getPicture(options).then(function(imageUri) {
        // Success! Image data is here
        if (dp == "iOS") {
            $scope.selfie.image = imageUri;
        } else {
            //$scope.boosterAddImageToCanvas(imageUri);
            $scope.uriToBase64(imageUri).then(function(result) {
                console.log('getPicture:uriToBase64:result=' + result);
                $scope.selfie.image = result;
                $scope.$apply();
            });
        }
    }, function(err) {
        // An error occured. Show a message to the user
        //alert('getPicture:err='+err);
        $rs.trackError(err, "getPicture");
    });

    var canvas, context;
    $scope.boosterAddImageToCanvas = function(imageUri) {
        canvas = document.getElementById('croppedImage');
        context = canvas.getContext('2d');
        make_base();

        function make_base() {
            base_image = new Image();
            base_image.src = imageUri;
            context.drawImage(base_image, 100, 100);
            $scope.$apply();
        }
    }

    $scope.sendSelfie = function(doShare) {
        $rs.trackEvent("sendSelfie");
        $rs.showLoading();
        Booster.sendSelfie($rs.details.BoostId, $scope.selfie.comment, $scope.selfie.image).then(function(result) {
                $rs.hideLoading();

                $rs.trackSendSelfie($scope.selfie.comment, $rs.details.BoostId);
                $cordovaToast.showShortCenter('Selfie sent successfully!').then(function(success) {
                    if (doShare == true) {
                        if ($rs.debug > 0)
                            console.log('TakeSelfieBoostCtrl:sendSelfie:fbShare:$scope.selfie.UserId' + $scope.selfie.UserId);
                        var selfie = $scope.selfie;
                        selfie.ImageData = selfie.image;
                        $scope.details = $rs.details;
                        selfie.message = "@" + $rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
                        $rs.fbShare(selfie).then(function(result) {
                            $rs.trackEvent('sendSelfie:fbShare:result');
                            console.log("$state.go('app.completed')");
                            $state.go('app.completed');
                        });
                    }
                    console.log("$state.go('app.completed')");
                    $state.go('app.completed');
                }, function(error) {
                    // error
                    $rs.trackError(error, "showShortCenter:send_selfie");
                });
            },
            function(errorMessage) {
                $rs.hideLoading()
                $rs.trackError(errorMessage, "sendSelfie");
                if ($rs.debug > 0)
                    console.log("Error", "Error occured when sending selfie");
            });
    }

    $scope.uriToBase64 = function(fileUri) {
        var d = $q.defer();
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            var fullPath = fileUri;
            var relPath = fullPath.replace('file:\/\/\/storage\/emulated\/0\/', '');
            // REMOVE UP TO Android
            window.resolveLocalFileSystemURI(relPath, winConvert, failConvert);

            function winConvert(uri) {
                console.log('uriToBase64:winConvert2:uri=' + uri);
            }

            function failConvert(error) {
                console.log('uriToBase64:failConvert:error.code=' + error.code);
                d.reject(error);
            }
            fileSystem.root.getFile(relPath, null, gotFileEntry, fail);
        }

        function gotFileEntry(fileEntry) {
            console.log('uriToBase64:gotFileEntry:fileEntry=' + fileEntry);
            fileEntry.file(gotFile, fail);
        }

        function gotFile(file) {
            console.log('gotFile');
            readDataUrl(file);
        }

        function readDataUrl(file) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
                //console.log('uriToBase64:readDataUrl:evt.target.result='+evt.target.result);
                var base64Image = evt.target.result.replace('data:image\/jpeg;base64,', '');
                $scope.selfie.image = base64Image;
                //console.log('uriToBase64:readDataUrl:$scope.selfie.image='+$scope.selfie.image);
                $scope.$apply();
                d.resolve(base64Image);
                return d.promise;
            };
            reader.readAsDataURL(file);
        }

        function fail(error) {
            console.log('uriToBase64:fail:error.code=' + error.code);
        }
        return d.promise;
    }

})

.controller('EditBoostCtrl', function($scope, $rootScope, $ionicSideMenuDelegate) {
    $rs.trackEvent("EditBoostCtrl");
})
