angular.module('boosterapp.utility', ['ionic'])

.service('Utility', function($ionicPopup, $q, $rootScope, $state, $cordovaToast, Booster) {

  var $rs = $rootScope;

  var use_analytics = false;

  //var self = this;    
  var debug = 3; // debug level - higher means more verbose messages
  $rs.popup = function(title, message) {
    console.log('popup:title=' + title + ' ;message=' + message);
    /*
     $ionicPopup.show({
         title: title,
         template: message
     });
    */
  }

  $rs.goToState = function(theState) {
    $state.go(theState);
  }

  $rs.doDelayed = function(f1, delay, args) {
    console.log('doDelayed:args=' + JSON.stringify(args));

    function sleep(millis, callback) {
      setTimeout(function() {
        callback();
      }, millis);
    }

    function foobar_cont() {
      console.log("foobar_cont");
      console.log('doDelayed:foobar_cont:args=' + JSON.stringify(args));
      f1.apply(this, args);
    };
    sleep(delay, foobar_cont);
  }

  $rs.fbShare = function(selfie) {
    var d = $q.defer();
    console.log('$rs.fbShare:selfie.UserId=' + selfie.UserId);
    $rs.trackEvent("fbShare");

    var message = selfie.message, //selfie.BoostTitle,
      subject = 'Booster App',
      fileOrFileArray = "data:image/png;base64," + selfie.ImageData,
      url = "http://signup.theboosterapp.com";

    var sharing = window.plugins.socialsharing;
    sharing.share(
      message,
      subject,
      fileOrFileArray,
      url,
      function(result) {
        $rs.trackEvent('fbShare:success');
        console.log('Sharing success: ' + JSON.stringify(result));
        d.resolve(result);
        return d.promise;
      },
      function(result) {
        $rs.trackError(error, "fbShare");
        console.log('Sharing error: ' + JSON.stringify(result));
        d.reject(result);
        return d.promise;
      });
    return d.promise;
  }

  //FB login button click event
  $rs.promiseFacebookLogin = function() {
    $rs.trackEvent('promiseFacebookLogin');
    console.log('promiseFacebookLogin');
    var fbId = $rs.getFBUserId();
    var fbLr = $rs.getFbLr();
    console.log('promiseFacebookLogin:2:fbLr=' + $rs.jStr(fbLr));
    var d = $q.defer();
    //var fbId = window.localStorage['fbid'];

    /*
    if ($rs.varIsSet(fbId) && $rs.varIsSet(fbLr)) {
      $rs.trackEvent('promiseGetLoginStatus:have:fbId&&fbLr)');
      mixpanel.identify(fbId);
      console.log('promiseFacebookLogin:resolve:fbId=' + fbId);
      console.log('promiseFacebookLogin:resolve:fbLr=' + fbLr);
      d.resolve(fbId);
      return d.promise;
    }
    */

    /*
    console.log('window.cordova='+window.cordova);
    if (!window.cordova) {
        //facebookConnectPlugin = fbcpWeb;
        facebookConnectPlugin.browserInit("625654127541709");
        // version is optional. It refers to the version of API you may want to use.
    }
    */


    if ($rs.debug > 3) console.log('promiseFacebookLogin:call:facebookConnectPlugin');
    facebookConnectPlugin.
    login
      (
        ['public_profile', 'user_friends', 'email'],
        // SUCCESS
        function(res) {
          if ($rs.debug > 2) console.log('facebookConnectPlugin.login:success');
          $rs.trackEvent('facebookConnectPlugin.login:success');
          if ($rs.debug > 4) console.log("promiseFacebookLogin:res=" + $rs.jStr(res));
          $rs.setFbLr(res);
          d.resolve(res);
          return d.promise;
        },
        function(err) {
          console.log('facebookConnectPlugin.login:fail');
          $rs.trackEvent('facebookConnectPlugin.login:fail');
          var errLog = 'promiseFacebookLogin:facebookConnectPlugin:err' + err;
          console.log(errorLog);
          d.reject(errLog);
          return d.promise;
        }
      )
    console.log('promiseFacebookLogin:end');
    return d.promise;
  };

  /*
  // WARNING: BREAKS PLUGIN SOMETIMES
  $rs.promiseGetLoginStatus = function()
  {
      $rs.trackEvent('promiseGetLoginStatus');
      console.log('promiseGetLoginStatus');
      var d = $q.defer();
      console.log('promiseGetLoginStatus:d='+d);
      //d.notify('promiseGetLoginStatus');

      var fbLr = $rs.getFbLr();
      console.log('promiseGetLoginStatus:fbLr='+fbLr);
      if($rs.varIsSet(fbLr)) 
      {
          $rs.trackEvent('promiseGetLoginStatus:$rs.varIsSet(fbLr)');
          d.resolve(fbLr);
          return d.promise;
      }

      facebookConnectPlugin.getLoginStatus
      (
          function(res)
          {
              $rs.trackEvent('facebookConnectPlugin.getLoginStatus:success');
              var fbId;
              console.log('promiseGetLoginStatus:getLoginStatus:res='+$rs.jStr(res));
              if(res.status + "" == "connected")
              { 
                  $rs.trackEvent('facebookConnectPlugin.getLoginStatus==connected');
                  fbId = res.authResponse.userID;
                  if($rs.varIsSet(fbId))
                  {
                      $rs.setFBUserId(fbId);
                      console.log('promiseGetLoginStatus:getLoginStatus:resolve:fbId='+fbId);
                      d.resolve(res);
                      return d.promise;
                  }
                  else
                  {
                      console.log('promiseGetLoginStatus:getLoginStatus:reject:fbId=='+fbId);
                      d.reject();
                      return d.promise;
                  }
              } else
              {
                  console.log('promiseGetLoginStatus:getLoginStatus:reject:connected==false');
                  d.reject();
                  return d.promise;
              }
          },
          function(err){
              $rs.trackEvent('facebookConnectPlugin.getLoginStatus:err='+err);
              console.log('facebookConnectPlugin.getLoginStatus:err='+err);
          }
      )
      console.log('promiseGetLoginStatus:getLoginStatus:return');
      return d.promise;
  };
  */

  /*
  $rs.promiseGetProfile = function() {
    console.log('promiseGetProfile');
    var d = $q.defer();
    var fbId = $rs.getFBUserId();
    console.log('promiseGetProfile:fbId=' + fbId);
    if (!$rs.varIsSet(fbId)) {
      console.log('promiseGetProfile:reject:fbId=' + fbId);
      d.reject('fbId=' + fbId);
      return d.promise;
    }
    Booster.getProfileByFbId(fbId)
      .then(function(result) {
        console.log('promiseGetProfile:getProfileByFbId:result=' + $rs.jStr(result));
        $rs.getProfileRes = result;
        if ($rs.varIsSet(result.data)) {
          console.log('promiseGetProfile:getProfileByFbId:resolve');
          $rs.setProfileFromWs(result);
          d.resolve(result);
          return d.promise;
        } else {
          console.log('promiseGetProfile:getProfileByFbId:reject');
          d.reject(result);
          /*
          console.log('promiseGetProfile:return:promiseRegister');
          return $rs.promiseRegister()
          .then(function(result)
          {
              console.log('promiseGetProfile:return:promiseGetProfile');
              return $rs.promiseGetProfile();
              //d.reject(result);
          });
          */
          /*
        }
      }).catch(function(err) {
        console.log('catch:promiseGetProfile:getProfileByFbId:reject');
        d.reject(err);
        return d.promise;
      });
    return d.promise;
  }
  */

  $rs.promiseLoginOrRegister = function() {
    var d = $q.defer();
    var fbLr = $rs.getFbLr();
    if (!$rs.varIsSet(fbLr)) {
      console.log('promiseLoginOrRegister:reject:fbLr=' + JSON.stringify(fbLr));
      d.reject(data);
      return d.promise;
    }
    var data = fbLr.authResponse;
    console.log('promiseLoginOrRegister:data=' + $rs.jStr(data));
    console.log('data.id=' + data.id + ' data.accessToken=' + data.accessToken);
    Booster.LoginOrRegister(data.accessToken)
      .then(function(result) {
          if ($rs.debug > 4) console.log('promiseLoginOrRegister:LoginOrRegister:result=' + JSON.stringify(result));
          if ($rs.varIsSet(result.data.UserId)) {
            var data = result.data;
            if ($rs.debug > 2) console.log("LoginOrRegister:data.FirstName=" + data.FirstName);
            $rs.setProfileFromWs(result);
            console.log('promiseLoginOrRegister:LoginOrRegister:resolve');
            d.resolve(result);
            return d.promise;
          } else {
            console.log('promiseLoginOrRegister:LoginOrRegister:fail');
            d.reject(result);
            return d.promise;
          }
        },
        function(err) {
          console.log('promiseLoginOrRegister:LoginOrRegister:reject:err=' + JSON.stringify(err));
          d.reject(err);
          return d.promise;
        });
    return d.promise;
  }

  $rs.doPromiseStartAppOnce = function() {
    console.log('doPromiseStartAppOnce');
    var d = $q.defer();
    if ($rs.alreadyStarting == true) {
      window.localStorage['isFirstEntrance'] = false;
      d.reject($rs.alreadyStarting);
      return false;
    }
    $rs.alreadyStarting = true;
    // TODO: need this but js wasn't finding it, even though I'm merely defining it here
    $rs.startAppCallCount = 1;
    //console.log('doPromiseStartAppOnce:startAppCallCount='+startAppCallCount);
    window.localStorage['isFirstEntrance'] = false;

    //GAB: works to speed up upcoming but shows error, missing profile info
    //$state.go('app.upcoming');

    return $rs.promiseStartApp();
    return d.promise;
  }

  // RECURSIVE WORK BACKWARDS
  $rs.promiseStartApp = function() {
    if ($rs.debug > 2) console.log('promiseStartApp');
    var d = $q.defer();
    var doStart = true;
    if ($rs.debug > 2) console.log('promiseStartApp:call:promiseLoginOrRegister');
    return $rs.promiseLoginOrRegister()
      .catch(function(err) {
        doStart = false;
        if ($rs.debug > 2) console.log('promiseStartApp:promiseLoginOrRegister:catch');
        return $rs.promiseFacebookLogin()
      }).finally(function(result) {
        if ($rs.debug > 2) console.log('promiseStartApp:finally:doStart=' + doStart);
        if (doStart) {
          if ($rs.debug > 2) console.log('promiseStartApp:finally:call:startUpcoming');
          $rs.startUpcoming();
          if ($rs.varIsSet($rs.pushState)) {
            $state.go($rs.pushState);
            $rs.pushState = "";
          }
          $rs.isAppStarted = true;
          d.resolve(result);
          return d.promise;
        } else {
          // RECURSE
          if ($rs.debug > 2) console.log('promiseStartApp:call:self:RECURSE:result=' + result);
          // TODO, some prob with this startAppCallCount variable
          if ($rs.startAppCallCount > 10) {
            if ($rs.debug > 2) console.log('promiseStartApp:call:self:RECURSE:startAppCallCount=' + startAppCallCount + ' result=' + result);
            d.reject(result);
            return d.promise;
          }
          $rs.startAppCallCount++;
          $rs.promiseStartApp();
        }
      });
    return d.promise;
  };

  $rs.startUpcoming = function() {
    if ($rs.debug > 2) console.log('startUpcoming');
    var getProfileWsr = $rs.getProfileFromWs();
    if ($rs.debug > 3) console.log('startUpcoming:getProfileWsr=' + getProfileWsr);
    getProfileWsr = JSON.parse(getProfileWsr);
    $rs.setUserId(getProfileWsr.data.UserId);
    $rs.updateDeviceId();
    $rs.setProfileFromWs(getProfileWsr);
    $state.go('app.upcoming');
  }

  $rs.varIsSet = function(x) {
    if (x == "" || x == null || x == undefined || x == "null" || x == "undefined") {
      return false;
    }
    return true;
  }

  //Update device id  
  $rs.updateDeviceId = function() {
    var userId = $rs.getUserId();
    //console.log('updateDeviceId:userId='+userId);
    $rs.trackEvent("updateDeviceId");
    var did = $rs.getDeviceId();
    console.log('updateDeviceId:userId' + userId + ' did=' + did);
    Booster.updateDeviceId(userId, did).then(function(response) {
        //Device updated successfully
        console.log('updateDeviceId:response=' + response);
        var test = response;
      },
      function(errorMessage) {
        $rs.trackError(errorMessage, "updateDeviceId");
        console.log('updateDeviceId:errorMessage=' + $rs.jStr(errorMessage));
        //$rs.popup("Error", "Error occured in updateDeviceId");
      }

    );
  };

  $rs.updateNotifications = function() {
    console.log('updateNotifications');
    Booster.getNotification($rs.getUserId()).then(function(result) {
        $rs.notef.count = result.data.UnReadCount;
        $rs.notef.list = result.data.Notifications;
      },
      function(errorMessage) {
        $rs.popup("Error", "Error: updateNotifications");
      });

    $rs.updateRequestCount();
  }

  $rs.updateRequestCount = function() {
    //Update request count 
    Booster.getRequestBoostCount($rs.getUserId()).then(function(result) {
        $rs.requestCount = result.data;

      },
      function(errorMessage) {
        $rs.popup("Error", "Error: updateRequestCount");
      });

  }

  $rs.setDevicePlatform = function(dp) {
    $rs.devicePlatform = dp;
  }

  $rs.getDevicePlatform = function() {
    //console.log('getDevicePlatform='+$rs.devicePlatform);
    return $rs.devicePlatform;
  }

  $rs.trackSendSelfie = function(comment, boostId) // TODO: change to toUserId
    {
      if (use_analytics) analytics.trackEvent('Comment', $rs.profile.UserId, comment);
      if ($rs.varIsSet(ga)) ga('send', 'Comment', $rs.profile.UserId, comment);
    }

  $rs.trackReview = function(comment, isLovedIt) // TODO: add toUserId
    {
      if (use_analytics) analytics.trackEvent('Comment', $rs.profile.UserId, comment, isLovedIt);
      ga('send', 'Comment', $rs.profile.UserId, comment, isLovedIt);
    }

  $rs.trackEvent = function(act) {
    var d = "x";
    d = $rs.getDevicePlatform();
    if (d == "Android") d = "A";
    if (d == "iOS") d = "i";

    var page = d + ':' + act;
    console.log('trackEvent:page=' + page);
    if (use_analytics) analytics.trackView(page);

    //gaPlugin.trackPage(nativePluginResultHandler, nativePluginErrorHandler, page);

    console.log('mixpanel.track:' + d + ":" + act);
    mixpanel.track(d + ":" + act);

    if ($rs.varIsSet(ga)) {
      ga('send', 'screenview', {
        'screenName': act
      });

      //GA 
      ga('send', 'screenview', {
        'appName': 'Booster_WebApp',
        'appId': 'myAppId',
        'appVersion': '1.6.6',
        'appInstallerId': 'myInstallerId',
        'screenName': act
      });

      ga('send', {
        'hitType': 'pageview',
        'page': act,
        'title': act
      });
    }
  }

  $rs.bulog = function(msg) {
    console.log("ul:" + msg);
    /*
    $cordovaToast.showShortTop('Here is a message').then(function(success) {
        // success
    }, function (error) {
        // error
    });
    */
  }

  $rs.trackError = function(screenName, errorMessage) {
    $rs.trackEvent("Error:" + screenName + ':' + errorMessage);
  }

  $rs.trackUserError = function(act) {
    $rs.trackEvent("UserError:" + act);
  }

  $rs.getFBUserId = function() {
    return window.localStorage['fbid'];
  };

  $rs.setFBUserId = function(fbid) {
    //console.log('setFBUserId:fbid='+fbid);  
    window.localStorage['fbid'] = fbid;
  };

  $rs.getFbFriends = function() {
    var fbFriends = window.localStorage['fbFriends'];
    //console.log('getFbFriends:fbFriends='+fbFriends);             
    return fbFriends;
  };

  $rs.setFbFriends = function(fbFriends) {
    //console.log('setFbFriends:fbFriends='+$rs.jStr(fbFriends));             
    window.localStorage['fbFriends'] = $rs.jStr(fbFriends);
  };

  $rs.getUserId = function() {
    return window.localStorage['userId'];
  };

  $rs.setUserId = function(userId) {
    window.localStorage['userId'] = userId;
  };

  $rs.getProfileFromWs = function() {
    if ($rs.debug > 4) console.log('getProfileWsr');
    var profile = window.localStorage['profile'];
    if ($rs.debug > 4) console.log('profile=' + profile);
    return profile;
  };

  /*
   ** @result = ws result
   */
  $rs.setProfileFromWs = function(getProfileWsr) {
    if ($rs.debug > 4) console.log('setProfileFromWs:getProfileWsr=' + getProfileWsr);
    try {
      var profileStr = JSON.stringify(getProfileWsr);
      if ($rs.debug > 4) console.log('setProfileFromWs:profileStr=' + profileStr);
      window.localStorage['profile'] = profileStr;
    } catch (err) {
      console.log('setProfileFromWs:catch:err=' + err);
    }
    $rs.profile = getProfileWsr.data;
    //$rs.profile.name = $rs.profile.FirstName + $rs.profile.LastName;
    //$rs.profile.points = $rs.profile.GiveBoostPoint + $rs.profile.GetBoostPoint;
    if ($rs.debug > 4) console.log('setProfileFromWs:$rs.profile=' + JSON.stringify($rs.profile));
    $rs.setMixpanelProfile(getProfileWsr);
    if (use_analytics) $rs.setGaProfile();
  };

  $rs.setGaProfile = function() {
    var fbid = $rs.getFBUserId();
    console.log('setGaProfile:fbid=' + fbid);
    analytics.setUserId(fbid);

    var UserId = $rs.getUserId();
    analytics.addCustomDimension(1, UserId, function(response) {
        console.log('setGaProfile:user-id:addCustomDimension:success:result=' + result);
      },
      function(error) {
        console.log('setGaProfile:user-id:addCustomDimension:fail:result=' + result);
      });

    console.log('setGaProfile:call:addCustomDimension:fbid=' + fbid);
    analytics.addCustomDimension(2, fbid, function(response) {
        console.log('setGaProfile:fbid:addCustomDimension:success:result=' + result);
      },
      function(error) {
        console.log('setGaProfile:fbid:addCustomDimension:fail:result=' + result);
      });

    ga('set', 'fbid', fbid);
    ga('set', 'User ID', UserId);
  }

  $rs.setMixpanelProfile = function(data) {
    $rs.userInfo = {
      "name": $rs.profile.name,
      "fbid": window.localStorage['fbid'],
      "userId": window.localStorage['userId'],
      "points": $rs.profile.points,
      "level": $rs.profile.level,
      "getBoostPt": $rs.profile.getBoostPt,
      "giveBoostPt": $rs.profile.giveBoostPt,
      "pic": $rs.profile.pic,
    }
    $rs.userDeviceInfo = {
      "device_platform": ionic.Platform.device().platform,
      "dp_version": ionic.Platform.device().version,
      "cordova_version": ionic.Platform.device().cordova,
      "uuid": ionic.Platform.device().uuid,
      "device_model": ionic.Platform.device().model,
      "Platform_connection": ionic.Platform.connection,
      "Platform_platforms": JSON.stringify(ionic.Platform.platforms),
      "Platform_grade": ionic.Platform.grade,
      "Platform_ua": ionic.Platform.ua,
      "isWebView": ionic.Platform.isWebView(),
      "isIPad": ionic.Platform.isIPad()
    }
    mixpanel.people.set($rs.userInfo);

    var fbid = $rs.getFBUserId();
    mixpanel.identify(fbid);
    mixpanel.alias(fbid);

    mixpanel.register_once({
      "userInfo": $rs.userInfo,
    });

    mixpanel.register({
      "userDeviceInfo": $rs.userDeviceInfo
    });
  }

  $rs.getFbLr = function() {
    var fbLrStr = window.localStorage['fbLr'];
    var fbLr;
    if ($rs.varIsSet(fbLrStr)) fbLr = JSON.parse(fbLrStr);
    return fbLr;
  }

  $rs.setFbLr = function(res) {
    console.log('setFbLr:res=' + $rs.jStr(res));
    //var fbId = res.authResponse.userID;
    var fbId = res.authResponse.id;
    console.log('setFbLr:fbId=' + fbId);
    $rs.setFBUserId(fbId);
    window.localStorage['fbLr'] = JSON.stringify(res);
  }

  $rs.setDeviceInfo = function(myDevice) {
    $rs.setDevicePlatform(myDevice.platform);
  };

  $rs.getDeviceId = function() {
    var deviceId = window.localStorage['did'];
    console.log('getDeviceId:deviceId=' + deviceId);
    return deviceId;
  };

  $rs.setDeviceId = function(deviceId) {
    $rs.myDeviceId = deviceId;
    var before = window.localStorage['did'];
    window.localStorage['did'] = deviceId;
    // If we just acquired a deviceId, where it was null before
    //if(!$rs.varIsSet(before) && $rs.varIsSet(deviceId)) 
    //{
    console.log('setDeviceId:updateDeviceId');
    $rs.updateDeviceId();
    //}
    console.log('setDeviceId:deviceId=' + deviceId);
  };

  $rs.jStr = function(jsonObj) {
    return JSON.stringify(jsonObj, null, "\t");
  }

  $rs.PEDNING = "PENDING";
  $rs.REQUEST = "REQUEST";
  $rs.CIMPLITED = "CIMPLITED"
});
