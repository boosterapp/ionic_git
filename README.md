## ready to use!

You do not need to do anything to build/run this

However, if you encounter problems, you can look into these scripts/activities

## pre-install
install plugins installed from local repos (submodules)
run install.sh

## android
import the fb lib from ../submodules/phonegap-facebook-plugin

## ios
might have to tick "Target Membership" for missing .m plugins (eg fb plugin, keyboard)

## uninstall plugins (only if you want to clean project or something)
run scripts/pluginUninstaller.sh

## re-install (not necessary unless you uninstalled some plugins)
run scripts/pluginInstaller.sh

## prepare for build/run
run prepare.sh
