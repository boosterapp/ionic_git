#bower install ngCordova
cordova platform add ios
cordova platform add android

#cordova plugin add https://github.com/ccsoft/cordova-facebook
cordova plugin add https://github.com/thedracle/cordova-android-chromeview.git
cordova plugin add org.apache.cordova.inappbrowser
cordova plugin add org.apache.cordova.console
cordova plugin add org.apache.cordova.camera
#cordova plugin add https://github.com/zhushunqing/cordova-camera-android-crop-available
cordova plugin add https://github.com/VitaliiBlagodir/cordova-plugin-datepicker.git
cordova plugin add org.apache.cordova.device
cordova plugin add org.apache.cordova.dialogs
cordova plugin add org.apache.cordova.file
cordova plugin add https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git
cordova plugin add org.apache.cordova.statusbar
cordova plugin add https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin.git
cordova plugin add org.apache.cordova.network-information
cordova plugin add https://github.com/danwilson/google-analytics-plugin.git

#NEED FOR IOS 7 NO KB
cordova plugin add https://github.com/driftyco/ionic-plugins-keyboard.git
#NEED FOR POPUPS FOR EG PUSH HANDLING
cordova plugin add de.appplant.cordova.plugin.local-notification
#USE OUR PUSH PLUGIN
cordova plugin add https://github.com/adaptivedev/PushPlugin.git

cordova platform add ios
cordova platform add android

#cordova -d plugin add submodules/adaptivedev/phonegap-facebook-plugin/ --variable APP_ID="625654127541709" --variable APP_NAME="BoosterApp"
cordova -d plugin add ../submodules/adaptivedev/phonegap-facebook-plugin/ --variable APP_ID="625654127541709" --variable APP_NAME="BoosterApp"


#cordova plugin add https://github.com/adaptivedev/cordova-facebook.git
#cordova plugin add https://github.com/christocracy/cordova-plugin-background-geolocation.git
#cordova plugin add org.apache.cordova.battery-status
#cordova plugin add org.apache.cordova.media-capture
#cordova plugin add org.apache.cordova.contacts
#cordova plugin add org.apache.cordova.geolocation
#cordova plugin add org.apache.cordova.globalization
#cordova plugin add https://github.com/SidneyS/cordova-plugin-nativeaudio.git
#cordova plugin add org.apache.cordova.media
#cordova plugin add https://github.com/Paldom/PinDialog.git
#cordova plugin add https://github.com/dferrell/plugins-application-preferences.git
#cordova plugin add org.pbernasconi.progressindicator
#cordova plugin add https://github.com/Paldom/SpinnerDialog.git
#cordova plugin add org.apache.cordova.vibration
#cordova plugin add https://github.com/MobileChromeApps/zip.git

##cordova plugin add https://github.com/ohh2ahh/AppAvailability.git
##cordova plugin add https://github.com/wildabeast/BarcodeScanner.git
##cordova plugin add https://github.com/VersoSolutions/CordovaClipboard
##cordova plugin add org.apache.cordova.device-motion
##cordova plugin add org.apache.cordova.device-orientation
##cordova plugin add org.apache.cordova.splashscreen
##cordova plugin add https://github.com/brodysoft/Cordova-SQLitePlugin.git
