cordova platform add android

cordova -d plugin add https://github.com/phonegap/phonegap-facebook-plugin.git --variable APP_ID="625654127541709" --variable APP_NAME="BoosterApp"

android update project --subprojects --path "platforms/android" --target android-19 --library "CordovaLib"

android update project --subprojects --path "platforms/android" --target android-19 --library "FacebookLib"

cd platforms/android/

ant clean

cd FacebookLib

ant clean

#open -e AndroidManifest.xml 

#change your minSdkVersion and your targetSdkVersion to your environment settings for me it was:
#<uses-sdk android:minSdkVersion="14" android:targetSdkVersion="17" />

#ant release

#cd ../../..

#cordova build android
