angular.module('boosterapp', ['ionic', 'ngCordova', 'boosterapp.utility', 'boosterapp.services', 'boosterapp.controllers'])

.run(function($ionicPlatform, Utility, $state, $rootScope, PushNotifications) {
    var $rs = $rootScope;
    $rs.debug = 19;

    $ionicPlatform.ready(function() {

        if ($rs.debug > 0) console.log("run:$ionicPlatform.ready:CONSOLE.LOG WORKS!");

        $rs.boosterDeviceReady();

    });

    $rs.boosterDeviceReady = function() {
        // SET DEVICE PLATFORM
        $rs.setDeviceInfo(ionic.Platform.device());
        var theDp = $rs.getDevicePlatform();

        if ($rs.debug > 0) console.log("run:theDp=" + JSON.stringify(theDp));

        // ANALYTICS
        if (typeof analytics !== "undefined") {
            analytics.startTrackerWithId("UA-53969784-1");
            //analytics.debugMode();
        } else {
            if ($rs.debug > 0) console.log("Google Analytics Unavailable");
        }

        // UPDATE INSTEAD
        $rs.boosterVersion = "v1.8.8";
        var installed_version = window.localStorage['version'];
        if ($rs.debug > 0) console.log('installed_version=' + installed_version);
        if (installed_version != $rs.boosterVersion) {
            $rs.trackEvent('upgrade:to:' + $rs.boosterVersion + ':from:' + installed_version);
            if ($rs.debug > 0) console.log('$rs.boosterVersion=' + $rs.boosterVersion);
            window.localStorage['version'] = $rs.boosterVersion;
        }
        $rs.waitWindowPluginsCount = 0;
        $rs.waitForWindowPluginsReady();
        $rs.intervalWaitForWindow = null;
    }

    $rs.waitForWindowPluginsReady = function() {
        // INIT PLUGINS
        if (!$rs.varIsSet(window.plugins)) {
            $rs.waitWindowPluginsCount++;
            if ($rs.waitWindowPluginsCount > 5) {
                clearInterval($rs.intervalWaitForWindow);
                console.log('waitForWindowPluginsReady:call:windowPluginsReady:1');
                $rs.windowPluginsReady();
            }
            $rs.intervalWaitForWindow = setInterval(function() {
                $rs.waitForWindowPluginsReady()
            }, 3000);
            console.log($rs.boosterVersion + 'waitForWindowPluginsReady:undefined:window.plugins=' + window.plugins + ':$rs.waitWindowPluginsCount=' + $rs.waitWindowPluginsCount);
            if ($rs.waitWindowPluginsCount > 0) {
                console.log($rs.boosterVersion + ':waitForWindowPluginsReady:undefined:window.plugins=' + window.plugins + ':$rs.waitWindowPluginsCount=' + $rs.waitWindowPluginsCount);
            }
        } else {
            clearInterval($rs.intervalWaitForWindow);
            console.log('waitForWindowPluginsReady:call:windowPluginsReady:2');
            $rs.windowPluginsReady();
            if ($rs.waitWindowPluginsCount > 1) {
                console.log($rs.boosterVersion + 'waitForWindowPluginsReady:clearInterval:window.plugins=' + window.plugins + ':$rs.waitWindowPluginsCount=' + $rs.waitWindowPluginsCount);
            }
        }
    }

    $rs.windowPluginsReady = function() {
        console.log('run:windowPluginsReady');
        // PUSH NOTIFICATIONS INIT
        PushNotifications.initialize();
        console.log('run:windowPluginsReady:after:PushNotifications.initialize()');
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        if ($rs.debug > 0) console.log('onDeviceReady:window.cordova.logger=' + window.cordova.logger);
        if ($rs.debug > 0) console.log('onDeviceReady:window.facebookConnectPlugin=' + window.facebookConnectPlugin);

        /* BREAKING iOS
        if($rs.getDevicePlatform() != "Android") 
        {
          StatusBar.setStyleForStatusBar(1);
          StatusBar.hide();
        }
        */

        // START APP
        console.log('windowPluginsReady:call:callPromiseStartAppOnce');
        $rs.callPromiseStartAppOnce();
    }

    $rs.callPromiseStartAppOnce = function() {
        console.log('callPromiseStartAppOnce');
        //alert('android working in LoadingCtrl!')
        var doAutoLogin = false;
        //if($rs.debug>0) console.log('isFirstEntrance='+window.localStorage['isFirstEntrance']);
        if (!$rs.varIsSet(window.localStorage['isFirstEntrance'])) {
            window.localStorage['isFirstEntrance'] = true;
            $rs.isFirstEntrance = true;
        }
        if (window.localStorage['isFirstEntrance'] == "false") {
            doAutoLogin = true;
        }
        if (doAutoLogin == true) {
            $rs.doPromiseStartAppOnce(); //.then(function(result) 
        } else {
            $state.go('app.login');
        }
    }

    /*
    // Wait for device API libraries to load
    //
    function onLoad() {
      console.log('run:onLoad');
      document.addEventListener("deviceready", onDeviceReady, false);
    }

    // device APIs are available
    //
    function onDeviceReady() {
      // Now safe to use device APIs
      alert('run:onDeviceReady');
      $rs.boosterDeviceReady();
    } 
    */

})

.config(function($stateProvider, $urlRouterProvider) {
    var baseUrl = "http://lin-res.mapitpix.com/Booster/ionic_git/www/";
    $stateProvider

        .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: baseUrl + "templates/menu.html",
        controller: 'MenuCtrl'
    })

    .state('app.loading', {
            url: "/loading",
            views: {
                'menuContent': {
                    templateUrl: baseUrl + "templates/loading.html",
                    controller: 'LoadingCtrl'
                }
            }
        })
        .state('app.loading2', {
            url: "/loading2",
            views: {
                'menuContent': {
                    templateUrl: baseUrl + "templates/loading2.html",
                    controller: 'Loading2Ctrl'
                }
            }
        })
        .state('app.login', {
            url: "/login",
            views: {
                'menuContent': {
                    templateUrl: baseUrl + "templates/login.html",
                    controller: 'LoginCtrl'
                }
            }
        })

    .state('app.choosebooster', {
        url: "/choosebooster",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/choosebooster.html",
                controller: 'ChooseBoostCtrl'
            }
        }
    })

    .state('app.selfie', {
        url: "/selfie/:boostId",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/selfie.html",
                controller: 'SelfieBoostCtrl'
            }
        }
    })

    .state('app.takeselfie', {
        url: "/takeselfie",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/takeselfie.html",
                controller: 'TakeSelfieBoostCtrl'
            }
        }
    })

    .state('app.editboost', {
        url: "/editboost",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/editboost.html",
                controller: 'EditBoostCtrl'
            }
        }
    })

    .state('app.getboost', {
            url: "/getboost/:fbid",
            views: {
                'menuContent': {
                    templateUrl: baseUrl + "templates/getboost.html",
                    controller: 'GetBoostCtrl'
                }
            }
        })
        //          templateUrl: "http://res.theboosterapp.com/ionic_git/www/templates/upcoming.html",
        .state('app.upcoming', {
            url: "/upcoming",
            views: {
                'menuContent': {
                    templateUrl: baseUrl + "templates/upcoming.html",
                    controller: 'UpcomingCtrl'
                }
            }
        })

    .state('app.requests', {
        url: "/requests",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/requests.html",
                controller: 'RequestsCtrl'
            }
        }
    })

    .state('app.completed', {
        url: "/completed",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/completed.html",
                controller: 'CompletedCtrl'
            }
        }
    })

    .state('app.review', {
        url: "/review/:boostId/:hasAction",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/review.html",
                controller: 'ReviewCtrl'
            }
        }
    })

    .state('app.completedreview', {
        url: "/creview/:boostId/:hasAction",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/completedreview.html",
                controller: 'CompletedReviewCtrl'
            }
        }
    })

    .state('app.notification', {
        url: "/notification",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/notification.html",
                controller: 'NotificationCtrl'
            }
        }
    })

    .state('app.profile', {
        url: "/profile",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/profile.html",
                controller: 'ProfileCtrl'
            }
        }
    })

    .state('app.contact', {
        url: "/contact",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/contact.html"
            }
        }
    })

    .state('app.about', {
        url: "/about",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/about.html",
                controller: 'AboutCtrl'
            }
        }
    })

    .state('app.intro1', {
        url: "/intro1",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/intro1.html"
            }
        }
    })

    .state('app.intro2', {
        url: "/intro2",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/intro2.html"
            }
        }
    })

    .state('app.intro3', {
        url: "/intro3",
        views: {
            'menuContent': {
                templateUrl: baseUrl + "templates/intro3.html"
            }
        }
    })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/loading');
});
